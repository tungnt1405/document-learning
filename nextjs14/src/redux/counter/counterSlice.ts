import { createSlice } from '@reduxjs/toolkit';
import { initialState } from './state';
import reducers from './reducer';

export const counterSlice = createSlice({// initialize state
    name: 'counter',
    initialState,
    reducers: reducers,
});

export default counterSlice.reducer;
