import { counterSlice } from './counterSlice';

export const { increment, decrement, incrementByAmount } = counterSlice.actions; //mutation

export const incrementAsync = (amount: number) => (dispatch: any) => {//actions
    setTimeout(() => {
        dispatch(incrementByAmount(amount));
    }, 1000);
};
