import { RootState } from '../store';
//getter
export const selectCount = (state: RootState) => state.counter.value;