import { PayloadAction } from '@reduxjs/toolkit';
import { PostState } from './state';

const setPosts = (state: PostState, action: PayloadAction<any[]>) => {
    state.posts = [...action.payload];
};

const setPost = (state: PostState, action: PayloadAction<object>) => {
    state.post = { ...action.payload };
};

const reducers = {
    setPosts,
    setPost
};

export default reducers;
