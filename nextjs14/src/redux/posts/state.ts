export interface PostState {
    posts: any[];
    post: {};
}

export const initialState: PostState = {
    posts: [],
    post: {},
};
