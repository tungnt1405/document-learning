import { RootState } from '../store';
//getter
export const getPosts = (state: RootState) => state.article.posts;
export const getPost = (state: RootState) => state.article.post;