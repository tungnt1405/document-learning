import { postSlice } from './postSlice';

export const { setPosts, setPost } = postSlice.actions; //mutation

export const getPostsAsync = () => async (dispatch: any) => {
    const res = await fetch('https://jsonplaceholder.typicode.com/posts');
    const result = await res.json();
    return dispatch(setPosts(result));
};
