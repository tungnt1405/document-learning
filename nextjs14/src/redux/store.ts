import { configureStore } from '@reduxjs/toolkit';
import counterReducer from './counter/counterSlice';
import postReducer from './posts/postSlice';

export const store = configureStore({
    reducer: { counter: counterReducer, article: postReducer },
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
