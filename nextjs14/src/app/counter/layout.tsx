import './counter.css';

export default function CounterLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return <div>{children}</div>;
}
