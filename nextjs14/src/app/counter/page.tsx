'use client';

import React from 'react';
import { useAppDispatch, useAppSelector } from '@/hook/redux-hook';
import { selectCount } from '@/redux/counter/selector';
import { decrement, increment, incrementAsync } from '@/redux/counter/actions';

export default function CounterPage() {
    const count = useAppSelector(selectCount);
    const dispatch = useAppDispatch();

    return (
        <div>
            <button onClick={() => dispatch(increment())}>increment</button>
            <button onClick={() => dispatch(incrementAsync(5))}>incrementAsync</button>
            <span>{count}</span>
            <button onClick={() => dispatch(decrement())}>decrement</button>
        </div>
    );
}
