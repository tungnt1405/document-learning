'use client';

import { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '@/hook/redux-hook';
import { getPostsAsync } from '@/redux/posts/actions';
import { getPosts } from '@/redux/posts/selector';

export default function PostPage() {
    const posts = useAppSelector(getPosts);
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(getPostsAsync());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div>
            {posts.map((post) => (
                <p key={post.id}>{`${post.id} - ${post.title}`}</p>
            ))}
        </div>
    );
}
