import React from "react";
import PageInterface from "../interfaces/page-interfaces";
import logging from "../configs/logging";
import { Box, Grid, Typography } from "@mui/material";
import UserMenu from "../components/Home/user-menu";
import FolderList from "../components/Home/folder/folder-list";
import { Outlet, useLoaderData } from "react-router-dom";
import FoldersInterface from "../interfaces/folders-interfaces";
import PushNotification from "../components/notification/PushNotification";

const Home: React.FunctionComponent<PageInterface> = (props) => {
  const { folders } = useLoaderData() as FoldersInterface;

  React.useEffect(() => {
    logging.info(`Loading ${props.name}`);
  }, []);
  return (
    <React.Suspense>
      <Typography variant="h4" sx={{ mb: "20px" }}>
        TODO application
      </Typography>
      <Box sx={{ display: "flex", justifyContent: "end", mb: "10px" }}>
        <UserMenu />
        <PushNotification />
      </Box>

      <Grid
        container
        sx={{ height: "50vh", boxShadow: "0 0 15px 0 rgb(193 193 193 / 60%)" }}
      >
        <Grid item xs={3} style={{ height: "100%" }}>
          <FolderList folders={folders} />
        </Grid>
        <Grid item xs={9} style={{ height: "100%" }}>
          <Outlet />
        </Grid>
      </Grid>
    </React.Suspense>
  );
};

export default Home;
