import { useRouteError } from "react-router-dom";
import ErrorInterface from "../interfaces/error-interfaces";

export default function ErrorPage() {
  const err = useRouteError() as ErrorInterface;
  console.error("error", err);

  return (
    <div id="error-page">
      <h1>Opps!...</h1>
      <p>Sorry, an unexcepted error has orrcured.</p>
      <p>
        <i>{err?.statusText || err?.message}</i>
      </p>
    </div>
  );
}
