import { Button, Typography } from "@mui/material";
import React, { useEffect } from "react";
import logging from "../configs/logging";
import { GoogleAuthProvider, getAuth, signInWithPopup } from "firebase/auth";
import { Navigate } from "react-router-dom";
import { requestUtil } from "../utils";

const Login: React.FunctionComponent<{}> = (props) => {
  const auth = getAuth();

  useEffect(() => {
    logging.info(`Loading ${props}`);
  }, []);

  const hanldeLoginWithGoogle = async () => {
    const provider = new GoogleAuthProvider();

    const {
      user: { uid, displayName },
    } = await signInWithPopup(auth, provider);

    const { data } = await requestUtil.graphqlRequest({
      query: `mutation Register($uid: String!, $name: String!) {
        register(uid: $uid, name: $name) {
          uid
          name
        }
      }`,
      variables: {
        uid,
        name: displayName,
      },
    });
    console.log("data", { data });
  };

  if (localStorage.getItem("accessToken")) {
    return <Navigate to="/" />;
  }

  return (
    <React.Suspense>
      <Typography variant="h5" sx={{ marginBottom: "10px" }}>
        Welcome to TODO-APP
      </Typography>
      <Button variant="outlined" onClick={hanldeLoginWithGoogle}>
        Login with Google
      </Button>
    </React.Suspense>
  );
};

export default Login;
