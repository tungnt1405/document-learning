export default interface NoteInterfaces {
    note: {
        id: number | string,
        content: string
    }
}