import React from "react";

export default interface RouteInterface {
  element: React.ReactNode;
  path?: string;
  children?: any[];
}
