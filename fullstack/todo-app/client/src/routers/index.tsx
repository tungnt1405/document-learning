import { createBrowserRouter, Outlet } from "react-router-dom";
import routes from "./routes";
import AuthProvider from "../context/AuthProvider";
import ErrorPage from "../pages/error";

const AuthLayout = () => {
  return (
    <AuthProvider>
      <Outlet />
    </AuthProvider>
  );
};

const router = createBrowserRouter([
  {
    element: <AuthLayout />,
    children: routes,
    errorElement: <ErrorPage />,
  },
]);

export default router;
