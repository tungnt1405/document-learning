import { NoteList } from "../components/Home/note-list";
import NoteDetail from "../components/Home/note-list/note-detail";
import RouteInterface from "../interfaces/route-interfaces";
import AuthMiddleware from "../middleware/AuthMiddleware";
import Home from "../pages";
import Login from "../pages/login";
import { addNewNote, folderLoader, noteDetailLoader, noteLoader, updateNewNote } from "../utils";

// tutorial: https://www.youtube.com/watch?v=J6jzDfHoj-Y&ab_channel=TheNerdyCanuck
const routes: RouteInterface[] = [
  {
    element: <AuthMiddleware />,
    children: [
      {
        element: <Home name="HomePage" />,
        path: "/",
        loader: folderLoader,
        children: [
          {
            element: <NoteList />,
            path: `folders/:folderId`,
            action: addNewNote, // update data
            loader: noteLoader, // get data
            children: [
              {
                element: <NoteDetail />,
                path: `note/:noteId`,
                action: updateNewNote,
                loader: noteDetailLoader,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    element: <Login />,
    path: "/login",
  },
];

export default routes;
