import { createClient } from "graphql-ws";
import NotificationsIcon from "@mui/icons-material/Notifications";
import { useEffect, useState } from "react";
import { GRAPHQL_SUBSCRIPTION_ENDPOINT } from "../../constants";
import { Badge, Menu, MenuItem } from "@mui/material";
import NotifyMessageInferface from "../../interfaces/notifyMessage-interfaces";

const client = createClient({
  url: GRAPHQL_SUBSCRIPTION_ENDPOINT,
});

const query = `subscription Notification {
    notification {
      message
    }
  }`;

export default function PushNotification() {
  const [invisible, setInvisible] = useState(true);
  const [notification, setNotification] = useState("");
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const isOpenMenu = Boolean(anchorEl);
  useEffect(() => {
    // subscription
    (async () => {
      const subscription = client.iterate({
        query: query,
      });

      for await (const event of subscription) {
        setInvisible(false);
        const { message } = event?.data?.notification as NotifyMessageInferface;
        setNotification(message);

        // complete a running subscription by breaking the iterator loop
        break;
      }
    })();
  }, []);

  const hanldeClick = (e: any) => {
    if (notification) {
      setAnchorEl(e.currentTarget);
    }
  };

  const hanldeClose = () => {
    setAnchorEl(null);
    setNotification("");
    setInvisible(true);
  };

  return (
    <>
      <Badge color="secondary" variant="dot" invisible={invisible}>
        <NotificationsIcon onClick={hanldeClick} />
      </Badge>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={isOpenMenu}
        onClose={() => setAnchorEl(null)}
      >
        <MenuItem onClick={hanldeClose}>{notification}</MenuItem>
      </Menu>
    </>
  );
}
