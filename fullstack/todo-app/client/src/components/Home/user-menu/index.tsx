import React from "react";
import { AuthContext } from "../../../context/AuthProvider";
import { Avatar, Box, Menu, MenuItem, Typography } from "@mui/material";

export default function UserMenu() {
  const {
    user: { displayName, photoURL, auth },
  } = React.useContext(AuthContext);

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const isOpenMenu = Boolean(anchorEl);

  const hanldeLogout = () => {
    auth.signOut();
  };

  return (
    <React.Fragment>
      <Box sx={{ display: "flex" }} onClick={e => setAnchorEl(e.currentTarget)}>
        <Typography>{displayName}</Typography>
        <Avatar
          alt={displayName}
          src={photoURL}
          sx={{ width: 24, height: 24, ml: "10px" }}
        />
      </Box>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={isOpenMenu}
        onClose={() => setAnchorEl(null)}
      >
        <MenuItem onClick={hanldeLogout}>Logout</MenuItem>
      </Menu>
    </React.Fragment>
  );
}
