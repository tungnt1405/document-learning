import {
  Box,
  Card,
  CardContent,
  Grid,
  IconButton,
  List,
  Tooltip,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import {
  Link,
  Outlet,
  useLoaderData,
  useNavigate,
  useParams,
  useSubmit,
} from "react-router-dom";
import FolderInterface from "../../../interfaces/folder-interfaces";
import { NoteAddOutlined } from "@mui/icons-material";
import moment from "moment";

export default function NoteList() {
  const { folder } = useLoaderData() as FolderInterface;
  const { noteId, folderId } = useParams();
  const [activeNote, setActiveNote] = useState(noteId);
  const submit = useSubmit();
  const navigate = useNavigate();

  const isActiveNote = (id: any) =>
    id === activeNote ? "rgb(255 211 140)" : "";

  const hanldeAddNewNote = () => {
    submit(
      {
        content: "",
        folderId: folderId || "",
      },
      { method: "post", action: `/folders/${folderId}` }
    );
  };

  useEffect(() => {
    if (noteId) {
      setActiveNote(noteId);
      return;
    }

    if (folder?.notes?.[0]) {
      navigate(`note/${folder.notes[0].id}`);
    }
  }, [noteId, folder.notes]);

  return (
    <Grid container height="100%">
      <Grid
        item
        xs={4}
        sx={{
          width: "100%",
          maxWidth: 360,
          bgcolor: "#f0ebe3",
          height: "100%",
          overflow: "auto",
          padding: "10px",
          textAlign: "left",
        }}
      >
        <List
          subheader={
            <Box
              display="flex"
              alignItems="center"
              justifyContent="space-between"
            >
              <Typography sx={{ fontWeight: "bold" }}>Note</Typography>
              <Tooltip title="Add Note" onClick={hanldeAddNewNote}>
                <IconButton size="small">
                  <NoteAddOutlined />
                </IconButton>
              </Tooltip>
            </Box>
          }
        >
          {folder?.notes.map(
            ({
              id,
              content,
              updatedAt,
            }: {
              id: any;
              content: string;
              updatedAt: any;
            }) => (
              <Link
                key={id}
                to={`note/${id}`}
                style={{ textDecoration: "none" }}
                onClick={() => setActiveNote(id)}
              >
                <Card sx={{ mb: "5px", backgroundColor: isActiveNote(id) }}>
                  <CardContent
                    sx={{ "&:las-child": { pb: "10px" }, padding: "10px" }}
                  >
                    <div
                      style={{ fontSize: "14px", fontWeight: "bold" }}
                      dangerouslySetInnerHTML={{
                        __html: `${content.substring(0, 30) || "Empty"}`,
                      }}
                    ></div>
                    <Typography sx={{ fontSize: "10px" }}>
                      {moment(updatedAt).format("YYYY/MM/DD h:mm:ss a")}
                    </Typography>
                  </CardContent>
                </Card>
              </Link>
            )
          )}
        </List>
      </Grid>
      <Grid item xs={8}>
        <Outlet />
      </Grid>
    </Grid>
  );
}
