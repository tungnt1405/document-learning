import { useEffect, useMemo, useState } from "react";
import {
  ContentState,
  EditorState,
  convertFromHTML,
  convertToRaw,
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import { useLoaderData, useLocation, useSubmit } from "react-router-dom";
import NoteInterfaces from "../../../interfaces/note-interfaces";
import { debounce } from "@mui/material";

export default function NoteDetail() {
  const [editorState, setEditorState] = useState(EditorState.createEmpty());
  const { note } = useLoaderData() as NoteInterfaces;
  const submit = useSubmit();
  const location = useLocation();
  const [rawHTML, setRawHTML] = useState(note?.content);

  useEffect(() => {
    const blocksFromHTML = convertFromHTML(note?.content);
    const state = ContentState.createFromBlockArray(
      blocksFromHTML.contentBlocks,
      blocksFromHTML.entityMap
    );

    setEditorState(EditorState.createWithContent(state));
  }, [note.id]);

  const deboundMemorized = useMemo(() => {
    return debounce((rawHTML, note, pathname) => {
      if (rawHTML === note.content) return;

      submit(
        { ...note, content: rawHTML },
        {
          method: "post",
          action: pathname,
        }
      );
    }, 1000);
  }, []);

  useEffect(() => {
    deboundMemorized(rawHTML, note, location.pathname);
  }, [rawHTML, location.pathname]);

  const hanldeOnChange = (e: any) => {
    setEditorState(e);
    setRawHTML(draftToHtml(convertToRaw(e.getCurrentContent())));
  };

  useEffect(() => {
    setRawHTML(note?.content);
  }, [note.content]);
  return (
    <Editor
      editorState={editorState}
      onEditorStateChange={(e) => hanldeOnChange(e)}
      placeholder="Write Something ..."
    ></Editor>
  );
}
