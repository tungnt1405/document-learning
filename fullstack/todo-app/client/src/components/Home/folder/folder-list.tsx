import React, { useState } from "react";
import FolderListInterface from "./folder-list-interface";
import { Box, Card, CardContent, List, Typography } from "@mui/material";
import { Link, useParams } from "react-router-dom";
import NewFolder from "./folder-new";

const FolderList: React.FunctionComponent<FolderListInterface> = ({
  folders,
}) => {
  const { folderId } = useParams();
  const [activeFolder, setActiveFolder] = useState(folderId);

  const isActiveFolder = (id: any) =>
    id === activeFolder ? "rgb(255 211 140)" : "";

  return (
    <List
      sx={{
        width: "100%",
        bgcolor: "#7D9D9C",
        height: "100%",
        padding: "10px",
        textAlign: "left",
        overflow: "auto",
      }}
      subheader={
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Typography sx={{ fontWeight: "bold", color: "#fff" }}>
            Folders
          </Typography>
          <NewFolder />
        </Box>
      }
    >
      {folders &&
        folders.map(({ id, name }) => {
          return (
            <Link
              key={id}
              to={`folders/${id}`}
              style={{
                textDecoration: "none",
              }}
              onClick={() => setActiveFolder(id)}
            >
              <Card sx={{ mb: "10px", backgroundColor: isActiveFolder(id) }}>
                <CardContent
                  sx={{ "&:last-child": { pb: "10px" }, padding: "10px" }}
                >
                  <Typography sx={{ fontSize: "16px", fontWeight: "bold" }}>
                    {name}
                  </Typography>
                </CardContent>
              </Card>
            </Link>
          );
        })}
    </List>
  );
};

export default FolderList;
