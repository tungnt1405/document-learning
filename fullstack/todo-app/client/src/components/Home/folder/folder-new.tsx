import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  TextField,
  Tooltip,
} from "@mui/material";
import { CreateNewFolderOutlined } from "@mui/icons-material";
import { useEffect, useState } from "react";
import { addNewFolder } from "../../../utils";
import { useNavigate, useSearchParams } from "react-router-dom";

export default function NewFolder() {
  const [newFolderName, setNewFolderName] = useState("");
  const [open, setOpen] = useState(false);
  const [searchParams, setSearchParams] = useSearchParams();
  const popupName = searchParams.get("popup");
  const navigate = useNavigate();

  const hanldeOnClick = () => {
    setSearchParams({ popup: "add-folder" });
  };
  const handleOnChange = (e: any) => {
    setNewFolderName(e.target.value);
  };
  const handleClose = () => {
    setNewFolderName("");
    navigate(-1);
  };
  const handleAddNew = async () => {
    const { addFolder } = await addNewFolder({ name: newFolderName });
    console.log({ addFolder });

    handleClose();
  };

  useEffect(() => {
    if (popupName === "add-folder") {
      setOpen(true);
      return;
    }

    setOpen(false);
  }, [popupName]);
  return (
    <div>
      <Tooltip title="Add Folder" onClick={hanldeOnClick}>
        <IconButton size="small">
          <CreateNewFolderOutlined sx={{ color: "#fff" }} />
        </IconButton>
      </Tooltip>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>New Folder</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            label="Folder name"
            fullWidth
            size="small"
            sx={{ width: "400px" }}
            autoComplete="off"
            value={newFolderName}
            onChange={handleOnChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleAddNew}>OK</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
