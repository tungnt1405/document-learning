// const GRAPHQL_URI = "http://localhost:4000";
const GRAPHQL_URI = "https://toapp-fullstack.onrender.com";
const GRAPHQL_SUBSCRIPTION_ENDPOINT = "wss://toapp-fullstack.onrender.com/graphql";

export { GRAPHQL_URI, GRAPHQL_SUBSCRIPTION_ENDPOINT };
