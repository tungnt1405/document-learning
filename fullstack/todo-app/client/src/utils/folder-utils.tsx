import { requestUtil } from ".";

const folderLoader = async () => {
  const query = `query Folders {
      folders {
        id
        name
        createAt
      }
    }`;
  const data = await requestUtil.graphqlRequest({ query });

  return data;
};

const addNewFolder = async (newFolder: any) => {
  const query = `mutation Mutation($name: String!) {
    addFolder(name: $name) {
      name
      author {
        name
      }
    }
  }`;

  const data = await requestUtil.graphqlRequest({
    query,
    variables: { name: newFolder?.name },
  });

  return data;
};

export { folderLoader, addNewFolder };
