import { GRAPHQL_URI } from "../constants";

const graphqlRequest = async (payload: object, options?: object) => {
  if (localStorage.getItem("accessToken")) {
    const res = await fetch(`${GRAPHQL_URI}/graphql`, {
      method: "POST",
      headers: {
        "content-type": "application/json;charset=UTF-8",
        accept: "application/json",
        authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        ...options,
      },
      body: JSON.stringify(payload),
    });

    if (!res.ok) {
      if (res.status == 403) {
        return null;
      }
    }
    const { data } = await res.json();

    return data;
  }

  return null;
};

export const requestUtil = {
  graphqlRequest,
};
