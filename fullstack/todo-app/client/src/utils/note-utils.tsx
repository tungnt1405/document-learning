import { requestUtil } from ".";

const noteLoader = async ({ params: { folderId } }: { params: any }) => {
  const query = `query Folder($folderId: String!) {
      folder(folderId: $folderId) {
        id
        name
        notes {
          id
          content
          updatedAt
        }
      }
    }`;

  const data = await requestUtil.graphqlRequest({
    query,
    variables: {
      folderId: folderId,
    },
  });
  return data;
};

const noteDetailLoader = async ({ params: { noteId } }: { params: any }) => {
  const query = `query Note($noteId: String) {
    note(noteId: $noteId) {
      content
      id
    }
  }`;

  const data = await requestUtil.graphqlRequest({
    query,
    variables: {
      noteId: noteId,
    },
  });
  return data;
};

const addNewNote = async ({
  request,
}: {
  request: any;
}) => {
  const newNote = await request.formData();
  const formDataObj: { [key: string]: any } = {};
  newNote.forEach((value: any, key: any) => {
    formDataObj[key] = value;
  });

  const query = `mutation Mutation($content: String!, $folderId: ID!) {
    addNote(content: $content, folderId: $folderId) {
      id
      content
    }
  }`;

  const { addNote } = await requestUtil.graphqlRequest({
    query,
    variables: formDataObj,
  });

  return addNote;
};

const updateNewNote = async ({
  request,
}: {
  request: any;
}) => {
  const updatedNote = await request.formData();
  const formDataObj: { [key: string]: any } = {};
  updatedNote.forEach((value: any, key: any) => {
    formDataObj[key] = value;
  });

  const query = `mutation Mutation($id: String!, $content: String!) {
    updateNote(id: $id, content: $content) {
      id
      content
    }
  }`;

  const { updateNote } = await requestUtil.graphqlRequest({
    query,
    variables: formDataObj,
  });

  return updateNote;
};
export { noteLoader, noteDetailLoader, addNewNote, updateNewNote };
