import { CircularProgress } from "@mui/material";
import { getAuth } from "firebase/auth";
import React, { createContext } from "react";
import { useNavigate } from "react-router-dom";

type Auth = {
  user: any;
  setUser: any;
};

export const AuthContext = createContext<Auth>({ user: {}, setUser: {} });

export default function AuthProvider({ children }: { children: any }) {
  const [user, setUser] = React.useState({});
  const [isLoading, setIsLoading] = React.useState(true);
  const navigate = useNavigate();

  const auth = getAuth();

  React.useEffect(() => {
    const unsubcribed = auth.onIdTokenChanged(async (user) => {
      if (user?.uid) {
        const accessToken = await user?.getIdToken();
        setUser(user);
        if (accessToken !== localStorage.getItem("accessToken")) {
          localStorage.setItem("accessToken", accessToken);
          window.location.reload();
        }
        setIsLoading(false);
        return;
      }

      setUser({});
      localStorage.clear();
      setIsLoading(false);
      navigate("/login");
    });

    return () => unsubcribed();
  }, [auth]);

  return (
    <AuthContext.Provider value={{ user, setUser }}>
      {isLoading ? <CircularProgress /> : children}
    </AuthContext.Provider>
  );
}
