import { GraphQLScalarType } from "graphql";
import { Author, Folder, Note, Notification } from "../models/entity/index.js";
import { PubSub } from "graphql-subscriptions";

const pubsub = new PubSub();

const Date = new GraphQLScalarType({
  name: "Date",
  parseValue(value) {
    return new Date(value);
  },
  serialize(value) {
    return value.toISOString();
  },
});

const Query = {
  folders: async (parent, args, context) => {
    // fakeData.folders
    const folders = await Folder.find({
      authorId: context.uid,
    }).sort({
      updatedAt: "desc",
    });
    return folders;
  },
  folder: async (parent, args) => {
    const folderId = args.folderId;
    const findFolder = await Folder.findOne({
      _id: folderId,
    });
    return findFolder;
  },
  note: async (parent, args) => {
    // return fakeData.notes.find(
    //   (note) => String(note.id) === String(args.noteId)
    // );
    const noteId = args.noteId;
    const note = await Note.findById(noteId);

    return note;
  },
};

const Mutation = {
  addFolder: async (parent, args, context) => {
    const new_folder = new Folder({ ...args, authorId: context.uid });
    pubsub.publish("FOLDER_CREATED", {
      folderCreated: {
        message: "A new folder",
      },
    });
    await new_folder.save();

    return new_folder;
  },
  addNote: async (parent, args, context) => {
    const new_note = new Note(args);
    await new_note.save();

    return new_note;
  },
  updateNote: async (parent, args) => {
    const noteId = args.id;
    const note = await Note.findByIdAndUpdate(noteId, args);

    return note;
  },
  register: async (parent, args) => {
    const find_user = await Author.findOne({ uid: args.uid });
    if (!find_user) {
      const new_user = new Author(args);
      await new_user.save();
      return new_user;
    }
    return find_user;
  },
  pushNotification: async (parent, args) => {
    const new_notification = new Notification(args);
    pubsub.publish("PUSH_NOTIFiCATION", {
      notification: {
        message: args.content,
      },
    });
    await new_notification.save();

    return { message: "SUCCESS" };
  },
};

const Subscription = {
  folderCreated: {
    subscribe: () => pubsub.asyncIterator(["FOLDER_CREATED", "NOTE_CREATED"]),
  },
  notification: {
    subscribe: () => pubsub.asyncIterator(["PUSH_NOTIFiCATION"]),
  },
};

const FolderQuery = {
  author: async (parent, args) => {
    const authorId = parent.authorId;
    const author = await Author.findOne({ uid: authorId });
    // return fakeData.authors.find((author) => author.id === authorId);
    return author;
  },
  notes: async (parent, args) => {
    // return fakeData.notes.filter(
    //   (note) => String(note.folderId) === String(parent.id)
    // );
    const notes = await Note.find({
      folderId: parent.id,
    }).sort({
      updatedAt: "desc",
    });
    return notes;
  },
};

// resolvers: xử lý và trả về data dựa trên query
export const resolvers = {
  Date,
  Query,
  Folder: FolderQuery,
  Mutation,
  Subscription,
};
