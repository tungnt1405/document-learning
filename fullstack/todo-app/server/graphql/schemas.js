// typeDefs: khai báo kiểu dữ liệu
// Query thực hiện truy vấn từ phía client
// Mutation: thực hiện update hoặc xóa data
// Subscription: thực realtime nếu có thay đổi từ server

// Trong Query gồm: field => datatypes. Trường hợp chưa có datatypes thì khai báo theo dạng
// type DataTypes { field => datatypes }
/**
 type Mutation {

 }

 type Subscription {
   
 }
 */

const Date = `scalar Date`;

const Folder = `type Folder {
    id: String!,
    name: String,
    createAt: String,
    author: Author,
    notes: [Note]
   }`;

const Author = `type Author {
    uid: String!,
    name: String
   }`;

const Note = `type Note {
    id: String!,
    content: String,
    updatedAt: Date
   }`;

const Query = `type Query {
    folders: [Folder],
    folder(folderId: String!): Folder,
    note(noteId : String): Note,
   }`;

const Mutation = `type Mutation {
   addFolder(name: String!): Folder,
   updateNote(id: String!, content: String!): Note,
   addNote(content: String, folderId: ID!): Note,
   register(uid: String!, name: String!): Author,
   pushNotification(content: String): Message
}`;

const Message = `type Message {
   message: String
}`;
const Subscription = `type Subscription {
   folderCreated: Message,
   notification: Message
}`;

export const typeDefs = `#graphql
   ${Date}
   ${Message}
   ${Folder}
   ${Author}
   ${Note}
   ${Query}
   ${Mutation}
   ${Subscription}
`;
