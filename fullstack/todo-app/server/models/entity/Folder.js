import mongoose from "mongoose";
import folderSchema from "../schema/FolderSchema.js";

const Folder = mongoose.model("folders", folderSchema);

export default Folder;

