import mongoose from "mongoose";
import noteSchema from "../schema/NoteSchema.js";

const Note = mongoose.model("Notes", noteSchema);

export default Note;

