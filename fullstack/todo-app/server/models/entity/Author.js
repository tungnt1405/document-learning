import mongoose from "mongoose";
import authorSchema from "../schema/AuthorSchema.js";

const Author = mongoose.model("authors", authorSchema);

export default Author;

