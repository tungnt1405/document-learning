import mongoose from "mongoose";
import notificationSchema from "../schema/NotificationSchema.js";


const Notification = mongoose.model("Notification", notificationSchema);

export default Notification;

