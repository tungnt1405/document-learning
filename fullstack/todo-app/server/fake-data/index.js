export default {
  authors: [
    { id: 1, name: "name1" },
    { id: 2, name: "name2" },
  ],
  folders: [
    {
      id: 1,
      name: "String",
      createAt: "2023-09-26T16:27:02Z",
      authorId: 1,
    },
    {
      id: 2,
      name: "String2",
      createAt: "2023-09-26T16:27:02Z",
      authorId: 1,
    },
    {
      id: 3,
      name: "String3",
      createAt: "2023-09-26T16:27:02Z",
      authorId: 2,
    },
  ],
  notes: [
    { id: 1, content: "<p>Testttt1</p>", folderId: "2" },
    { id: 2, content: "<p>Testttt2</p>", folderId: "1" },
    { id: 3, content: "<p>Testttt3</p>", folderId: "1" },
    { id: 3, content: "<p>Testttt4</p>", folderId: "2" },
    { id: 5, content: "<p>Testttt5</p>", folderId: "2" },
  ],
};
