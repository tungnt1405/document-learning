# Giới thiệu project
- đây là project sử dụng các công nghệ: react, nodejs, graphql, firebase, mongodb
- Mục đích build ứng dụng
  + để hiểu cách làm ứng dụng fullstack react + nodejs
  + tìm hiểu thêm về các database
  + tìm hiểu cơ chế realtime(graphQL hoặc firebase) khi không dùng socketio (tương tự như socketio)
  + hiểu được graphQL thay thế cho restAPI (có trong amplify aws)
  + hiểu về non-sql với mongodb (tương tự dynamodb)

- Nguồn tham khảo [Xem](https://www.youtube.com/watch?v=aM_XIWjxcYA&ab_channel=HoleTex)

# công nghệ
## Server folder

Công nghệ dùng:
1. @apollo/server: sử dụng connect graphQL 
2. express
3. cors
4. http
5. body-parser 

# Tham khảo

- link [git](https://github.com/holetexvn/note-app-holetex) hotex 