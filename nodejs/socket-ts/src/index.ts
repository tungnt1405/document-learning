import express from "express";
import { createServer } from "http";
import { Server } from "socket.io";

const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer, {
  cors: {
    origin: "*",
  },
});

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

io.on("connection", (socket) => {
  console.log("connect");
  socket.on("chat message", (msg) => {
    io.emit("chat message", msg);
  });
  //   socket.on("disconnect", () => {
  //     console.log("disconnect");
  //   });
});

httpServer.listen(3000, () => {
  console.log("http://localhost:3000");
});
