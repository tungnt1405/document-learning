Bài 1:
- Nodejs là gì?
- Tại sao nên dùng Nodejs?
- Tạo 1 module HTTP với nodejs

Trả lời:
1. Nodejs là gì?
+ Nodejs là một môi trường máy chủ mã nguồn mở(nền tảng platform). Được phát triển và xây dựng trên Javascript Runtime(V8 Javascript Engine) của chrome.
+ Nodejs sử dụng javascript trên máy chủ mà không cần thông qua ngôn ngữ khác như PHP, ASP(.NET), JAVA,...
+ Được tạo bởi Ryan Dahl và ra mắt từ 2009.
+ Nodejs có thể trên nhiều nền tảng khác nhau (Windowns, Linux, Mac OS,...)
2. Tại sao nên dùng Nodejs?
+ Nodejs sử dụng lập trình không đồng bộ (asynchronous programming).
+ Node.js cũng cho phép các nhà phát triển xử lý các yêu cầu I/O đa luồng một cách hiệu quả hơn, giúp tăng tốc độ xử lý các ứng dụng web.
+ So sánh xử lý yêu cầu tệp:
-------------------------------------------------------------------------------------
| Các ngôn ngữ server khác(PHP, ASP,..)   |              Nodejs                     |
| -----------------------                 |            ----------                   |
| 1.Gửi tác vụ đến hệ thống của máy tính  | 1.Gửi tác vụ đến hệ thống của máy tính  |
| 2.Đợi hệ thống mở và đọc tệp            | 2.Sẵn sàng xử lý các yêu cầu tiếp theo  |
| 3.Trả về yêu cầu cho người dùng         | 3.Khi hệ thống hoàn thành mở, đọc tệp   |
|                                         | máy chủ sẽ trả lại nội dung cho người   |
|                                         | dùng                                    |
| 4.Sẵn sàng xử lý các yêu cầu tiếp theo  |                                         |
-------------------------------------------------------------------------------------
==> Nodejs sẽ loại bỏ sự chờ đợi và tiếp tục làm phần tiếp theo từ đó giúp cải thiện hiệu suất của ứng dụng (rất hiệu quả về bộ nhớ)
+ Nodejs làm được: nội dung 1 trang động, có thể mở, đóng, đọc, ghi file trên máy chủ, thu thập dữ liệu biểu mẫu, CRUD trong database,...
+ Một số ứng dụng có thể kết hợp nodejs:
 + WS(Websocket Server): chat, game,...
 + Fast File Upload Client: ứng upload file tốc độ cao
 + Ads Server: máy chủ quảng cáo
 + Cloud Server: dịch vụ đám mây
 + RESTful API: các dịch vụ api.
 + Any Real-time Data Application: bất kỳ một ứng dụng nào có yêu cầu về tốc độ thời gian thực. 
   Micro Services: Ý tưởng của micro services là chia nhỏ một ứng dụng lớn thành các dịch vụ nhỏ và kết nối chúng lại với nhau. 
   Nodejs có thể làm tốt điều này.
3. Tạo module HTTP với nodejs (file index.js)