// Tham khảo: https://www.w3schools.com/nodejs/nodejs_http.asp
var http = require('http'); // built-in module called HTTP, which allows Node.js to transfer data over the Hyper Text Transfer Protocol (HTTP).

http.createServer((req, res) => {// Sử dụng createServer() phương pháp để tạo máy chủ HTTP
    res.writeHead(200, {'Content-Type': 'text/html'}); // writeHead trả về thông tin của header cho browser với mã trạng thái với 200 là Ok, đối số thứ 2 là 1 objext
    res.write("Hello world!"); // Nội dung trả về
    res.end(); // kết thúc phản hồi khi gọi vào HTTP
}).listen(1234); // port sử dụng thường là 80

console.log('Server running at http://127.0.0.1:1234/'); // Hiển thị thông báo chạy thành công trên command