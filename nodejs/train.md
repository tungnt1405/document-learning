# Mục lục

## **Train html,css**

1.  [Tìm hiểu HMTL. DOCTYPE là gì? Tại lại dùng.](#qa1)
2.  [SVG là gì? Cách dùng img svg?](#qa2)
3.  [Tìm hiểu CSS. Các loại import css có? cách dùng từng loại import.](#qa3)
4.  [Có mấy loại selectors? độ ưu tiên](#qa4)
5.  [Pseudo-class là gì? ví dụ](#qa5)
6.  [Css inheritance là gì? ví dụ](#qa6)
7.  [Css box model là gì? làm thế nào để size tổng của element không đổi khi thay border-width.](#qa7)
8.  [có những loại display nào? display flex là gì? cách dùng](#qa8)
9.  [position có mấy loại? z-index là gì? TH z-index lớn nằm dưới element nhỏ trong TH nào?](#qa9)
10. [Responsive là gì? cách style respon? lấy ví dụ respon < 1200px](#qa10)
11. [Có những cách nào để child element căn giữa parent element?](#qa11)
12. [Table của css và html khác nhau gì?](#qa12)
13. [vh, px, vw, em, rem khác nhau gì?](#qa13)
14. [set kích thước tối thiểu 1 element kiểu gì?](#qa14)

## **Train js**

1. [var, let, const phân biệt?](#qa15)
2. [có mấy loại data type?](#qa16)
3. [Điều gì xảy ra khai báo 1 biến:](#qa17)
4. [tạo 1 string và 1 number như thế nào](#qa18)
5. [map, filter, slice, includes,... làm quen 1 số method array cơ bản? ví dụ?](#qa19)
6. [this là gì? khi nào dùng this? ví dụ](#qa20)
7. [Regex là gì? khi nào dùng?](#qa21)
8. [Làm sao hanlde error trong js?](#qa22)
9. [Có mấy dạng scope?](#qa23)
10. [Hoisting là gì? ví dụ?](#qa24)
11. [Class trong js là gì? cách dùng? construct, methods là gì? inheritance là gì? khi nào dùng?](#qa25)
12. [JSON là gì? cách dùng? Tác dụng json trong js? ví dụ.](#qa26)
13. [Bất đồng bộ js là gì? Promise, Promise.all, Promise.allSetle là gì? ví dụ?](#qa27)
14. [Compare async/await và then/catch?](#qa28)
15. [Access DOM bằng những func gì? Nên access nhiều lần hay 1 lần? khi loop 1 array cần chú ý gì để tăng performance?](#qa29)
16. [DOM là gì? có mấy cách reference đến DOM? có mấy cách listen 1 event?](#qa30)
17. [DOM node là gì? tạo và sử dụng ntn?](#qa31)
18. [phân biệt DOM nodes và DOM elements](#qa32)
19. [ví dụ form validation js](#qa33)
20. [ES6 là gì? compare function và arrow function? Destructing assignment là gì?](#qa34)

## **Train react**

1.  [Reacjs là gì? Vai trò và tại sao lại dùng react.](#qa35)
2.  [Ưu điểm của react so với js?](#qa36)
3.  [JSX là gì? ví dụ?](#qa37)
4.  [JSX Conditional JSX rendering](#qa38)
5.  [React Components là gì? sử dụng làm gì? Component lifecycle là gì?](#qa39)
6.  [Tạo component class kiểu gì? ví dụ](#Q40)
7.  [Nesting component là gì? ví dụ](#Q41)
8.  [Tại sao cần ket khi render component list](#qa42)
9.  [Prop và State là gì? compare](#qa43)
10. [Có nên khởi tạo state của component từ props không? tại sao](#qa44)
11. [Controller Component là gì? Ứng dụng](#qa45)
12. [Làm sao để call 1 func sau khi compenent được render/update.](#qa46)
13. [Ref là gì, mục đích dùng](#qa47)
14. [Pros à Cons khi dùng Ref](#Q48)
15. [React Context là gì, sử dụng khi nào?](#qa49)

---

---

# **Train html,css**

## QA1

## Tìm hiểu HMTL. DOCTYPE là gì? Tại lại dùng.

**Trả lời:**

HTML (HyperText Markup Language) là ngôn ngữ đánh dấu siêu văn bản được sử dụng để xây dựng các trang web. Nó định nghĩa cấu trúc và nội dung của một trang web bằng cách sử dụng các phần tử (elements) và các thuộc tính (attributes) để xác định các thành phần khác nhau trên trang.

DOCTYPE là một khối mã đặc biệt được đặt ở đầu tài liệu HTML để xác định phiên bản HTML sử dụng và tiêu chuẩn mà tài liệu đang tuân thủ. Nó định rõ loại tài liệu (document type) mà trang web sử dụng và cung cấp cho trình duyệt thông tin cần thiết để hiển thị trang web một cách chính xác.

Dùng DOCTYPE đúng phiên bản HTML và tiêu chuẩn cần thiết là quan trọng để đảm bảo trang web hoạt động chính xác và hiển thị đúng trên các trình duyệt khác nhau. Ngoài ra, việc tuân thủ các tiêu chuẩn HTML cũng giúp trang web có hiệu suất tốt hơn, dễ bảo trì và tương thích với các công nghệ web hiện đại.

Khai báo: `<!DOCTYPE html>`

## QA2

### SVG là gì? Cách dùng img svg?

**Trả lời:**

Ảnh SVG (Scalable Vector Graphics) là một định dạng hình ảnh dựa trên XML, được sử dụng để biểu diễn đồ họa vector mở rộng. SVG sử dụng các đối tượng hình học, đường cong và màu sắc để tạo ra các hình ảnh có thể tự mở rộng và thu nhỏ mà không làm mất chất lượng.

Cách dùng:

- step 1: download img svg hoặc tạo 1 file svg
- step 2: sử dụng `img` của html để nhúng svg

## QA3

### Tìm hiểu CSS. Các loại import css có? cách dùng từng loại import.

**Trả lời:**

CSS (Cascading Style Sheets) là một ngôn ngữ đánh dấu được sử dụng để định dạng và trình bày các trang web. Nó cho phép bạn tạo ra các quy tắc và kiểu cho các phần tử HTML, như màu sắc, kích thước, vị trí và hiệu ứng.

Có 3 loại chính: inline, internal, external

- inline: là cách nhập CSS bằng cách sử dụng thuộc tính style trực tiếp trong phần tử HTML. ví dụ: `<p style="color: blue; font-size: 16px;">Đây là một đoạn văn bản có màu xanh và kích thước chữ là 16px.</p>`
- internal: Internal CSS là cách nhập CSS bằng cách sử dụng thẻ `<style> // css </style>`
- external: là cách nhập CSS bằng cách sử dụng một tệp CSS riêng biệt và liên kết nó với tệp HTML bằng thẻ `<link>`

## QA4

### Có mấy loại selectors? độ ưu tiên

**Trả lời:**

Có nhiều loại CSS selectors, mỗi loại có cách hoạt động và độ ưu tiên khác nhau. Dưới đây là một số loại CSS selectors phổ biến:

- Selector phần tử (Element Selector): Sử dụng tên thẻ HTML để chọn phần tử. Ví dụ: p chọn tất cả các thẻ `<p>`.

- Selector lớp (Class Selector): Sử dụng tên lớp để chọn các phần tử có cùng tên lớp. Ví dụ: .my-class chọn các phần tử có class="my-class".

- Selector ID: Sử dụng ID để chọn một phần tử duy nhất. Ví dụ: #my-id chọn phần tử có id="my-id".

- Selector thuộc tính (Attribute Selector): Sử dụng các thuộc tính của phần tử để chọn các phần tử có thuộc tính tương ứng. Ví dụ: `[type="text"]` chọn các phần tử input có type="text".

- Selector con (Descendant Selector): Sử dụng cấu trúc phần tử con để chọn các phần tử con của một phần tử khác. Ví dụ: div p chọn tất cả các thẻ `<p>` là con của thẻ `<div>`.

- Selector trực tiếp (Child Selector): Sử dụng dấu > để chọn các phần tử con trực tiếp của một phần tử khác. Ví dụ: div > p chọn tất cả các thẻ `<p>` là con trực tiếp của thẻ `<div>`.

Độ ưu tiên của CSS selectors được xác định bằng cách sử dụng các quy tắc ưu tiên. Các selectors có độ ưu tiên cao hơn sẽ ghi đè lên các selectors có độ ưu tiên thấp hơn. Các quy tắc ưu tiên bao gồm:

- Inline styles: Inline styles có độ ưu tiên cao nhất. Điều này có nghĩa là nếu một phần tử có style inline, nó sẽ ghi đè lên bất kỳ quy tắc CSS nào khác.

- ID selectors: ID selectors có độ ưu tiên cao hơn so với các loại selectors khác. Điều này có nghĩa là nếu một phần tử có ID selector, nó sẽ ghi đè lên các selectors khác.

- Class selectors, attribute selectors và pseudo-classes: Class selectors, attribute selectors và pseudo-classes có độ ưu tiên trung bình.

- Element selectors: Element selectors có độ ưu tiên thấp nhất và sẽ bị ghi đè bởi các selectors khác.

## QA5

### Pseudo-class là gì? ví dụ

**Trả lời:**

Pseudo-class là một loại chọn phần tử trong CSS, cho phép bạn áp dụng kiểu dáng và hiệu ứng cho các trạng thái hoặc vị trí đặc biệt của phần tử. Pseudo-class được ký hiệu bằng dấu hai chấm (:) và được thêm vào sau bộ chọn (selector) để chỉ định trạng thái hoặc vị trí cần áp dụng kiểu.

Các Pseudo-class hay dùng: `:hover, :active, :nth-child, :focus...`

ví dụ:

- `:hover` được áp dụng khi con trỏ chuột đi qua phần tử.

```css
a:hover {
  background-color: yellow;
  color: red;
}
```

- `:active` được áp dụng khi người dùng nhấp chuột vào phần tử.

```css
button:active {
  background-color: green;
}
```

- `:nth-child()` được sử dụng để chọn các phần tử con dựa trên vị trí của chúng trong phần tử cha.

```css
li:nth-child(odd) {
  color: red;
}

li:nth-child(even) {
  color: blue;
}
```

## QA6

### Css inheritance là gì? ví dụ

**Trả lời:**

CSS inheritance là khả năng các thuộc tính CSS được kế thừa từ phần tử cha đến các phần tử con bên trong. Khi một phần tử con không có giá trị cho một thuộc tính cụ thể, nó sẽ thừa hưởng giá trị của thuộc tính từ phần tử cha.

ví dụ:

```css
<head>
<style>
    .parent {
        color: blue;
        font-size: 20px;
    }

    .child {
        /* không có thuộc tính nào được định nghĩa */
    }
</style>
</head>
<body>
    <div class="parent">
        Đây là phần tử cha
        <p class="child">
            Đây là phần tử con
        </p>
    </div>
</body>
```

## QA7

### Css box model là gì? làm thế nào để size tổng của element không đổi khi thay border-width.

**Trả lời:**

CSS Box Model là một khái niệm cơ bản trong CSS, mô tả cách mà các phần tử HTML được bố trí và kích thước trong trình duyệt. Nó bao gồm các thành phần chính sau:

- Content: Nội dung thực tế của phần tử, chẳng hạn như văn bản hoặc hình ảnh.

- Padding: Khoảng cách giữa nội dung và viền. Padding không làm thay đổi kích thước tổng của phần tử.

- Border: Viền xung quanh phần tử. Border cũng không làm thay đổi kích thước tổng của phần tử.

- Margin: Khoảng cách giữa phần tử và các phần tử khác xung quanh. Margin cũng không làm thay đổi kích thước tổng của phần tử.

Để giữ cho tổng kích thước của phần tử không thay đổi khi bạn thay đổi border-width sử dụng `box-sizing` với thuộc tính `border-box. Kích thước tổng của phần tử sẽ tính cả padding và border vào kích thước tổng, và chỉ tính margin là khoảng cách bên ngoài phần tử.

## QA8

### Có những loại display nào? display flex là gì? cách dùng

**Trả lời:**

Có nhiều loại giá trị cho thuộc tính CSS display, nhưng dưới đây là một số loại phổ biến:

- block: Các phần tử sử dụng giá trị block được hiển thị trên một dòng riêng biệt và chiếm hết không gian theo chiều ngang. Ví dụ: `<div>, <p>, <h1> - <h6>`, và các phần tử định dạng văn bản khác.

- inline: Các phần tử sử dụng giá trị inline được hiển thị trên cùng một dòng và không chiếm hết không gian theo chiều ngang. Ví dụ: `<span>, <a>, <strong>`, và các phần tử văn bản khác.

- inline-block: Các phần tử sử dụng giá trị inline-block được hiển thị trên cùng một dòng, nhưng có thể chiếm không gian theo chiều ngang và chiều dọc. Ví dụ: `<input>, <button>`, và các phần tử văn bản khác khi muốn áp dụng kích thước và margin/padding.

- none: Các phần tử sử dụng giá trị none không được hiển thị trên trang và không chiếm không gian.

Một trong những giá trị phổ biến của thuộc tính display là flex. Khi áp dụng display: flex cho một container, nó sẽ tạo ra một flex container và biến các phần tử bên trong nó thành flex items. Flexbox là một công nghệ linh hoạt cho việc bố trí và căn chỉnh các phần tử trong một hàng hoặc một cột.

Để sử dụng display: flex, bạn cần áp dụng thuộc tính này cho container chứa các phần tử bạn muốn căn chỉnh.

Ví dụ:

```css
<!DOCTYPE html>
<html>
<head>
    <style>
        .container {
        display: flex;
        }

        .item {
        /* styles for flex items */
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="item">Item 1</div>
        <div class="item">Item 2</div>
        <div class="item">Item 3</div>
    </div>
</body>
</html>
```

## QA9

### position có mấy loại? z-index là gì? TH z-index lớn nằm dưới element nhỏ trong TH nào?

**Trả lời:**

Trong CSS, có 5 loại giá trị cho thuộc tính position: static, relative, absolute, fixed, và sticky.

- static: Đây là giá trị mặc định cho position. Các phần tử được hiển thị theo luồng bình thường của tài liệu và không bị ảnh hưởng bởi các thuộc tính top, right, bottom, left, và z-index.

- relative: Các phần tử với position: relative sẽ được dịch chuyển dựa trên vị trí ban đầu của chúng. Bạn có thể sử dụng các thuộc tính top, right, bottom, left để chỉ định việc dịch chuyển từ vị trí gốc.

- absolute: Các phần tử với position: absolute sẽ được dịch chuyển dựa trên vị trí của phần tử cha gần nhất có position khác static. Bạn có thể sử dụng các thuộc tính top, right, bottom, left để chỉ định vị trí của phần tử.

- fixed: Các phần tử với position: fixed sẽ được dịch chuyển dựa trên vị trí của cửa sổ trình duyệt. Phần tử sẽ luôn được hiển thị ở cùng một vị trí ngay cả khi trang được cuộn.

- sticky: Các phần tử với position: sticky sẽ hoạt động như relative cho đến một ngưỡng cuộn được định trước, sau đó nó sẽ hoạt động như fixed cho đến khi phần tử thoát khỏi vùng cuộn.

z-index là một thuộc tính CSS được sử dụng để xác định thứ tự hiển thị của các phần tử theo trục Z (chiều sâu). Giá trị của z-index chỉ định ưu tiên hiển thị của một phần tử so với các phần tử khác. Phần tử có z-index cao hơn sẽ được hiển thị phía trên các phần tử có z-index thấp hơn.

Các phần tử con bên trong phần tử nhỏ hơn vẫn sẽ nằm trên phần tử có z-index lớn hơn, miễn là phần tử nhỏ hơn có một position không phải là static (chẳng hạn relative, absolute, fixed, hoặc sticky), trong khi phần tử lớn hơn không có position hoặc có position: static. Điều này xảy ra vì position của phần tử nhỏ hơn tạo ra một ngữ cảnh stacking context (ngữ cảnh xếp chồng) riêng biệt và các phần tử con bên trong nó sẽ nằm trên phần tử có z-index lớn hơn, bất kể giá trị z-index của phần tử nhỏ hơn.

## QA10

### Responsive là gì? cách style respon? lấy ví dụ respon < 1200px

**Trả lời:**

Responsive design là một phương pháp thiết kế web để làm cho trang web phù hợp và tương thích với các kích thước và thiết bị khác nhau. Mục tiêu của responsive design là làm cho trang web có trải nghiệm tốt trên các thiết bị như máy tính để bàn, điện thoại di động và máy tính bảng.

Để style responsive cho trang web, chúng ta có thể sử dụng các media query trong CSS. Media query cho phép bạn áp dụng các style khác nhau dựa trên kích thước màn hình hoặc các thuộc tính khác của thiết bị.

ví dụ

```css
/* Style cho kích thước màn hình dưới 1200px */
@media (max-width: 1200px) {
  /* Các style áp dụng khi màn hình dưới 1200px */
  body {
    font-size: 14px;
  }
  .container {
    width: 80%;
  }
  /* ... */
}
```

## QA11

### Có những cách nào để child element căn giữa parent element?

**Trả lời:**

Sử dụng Flexbox: Đối với parent element, ta có thể đặt thuộc tính display: flex và justify-content: center để căn giữa theo chiều ngang và align-items: center để căn giữa theo chiều dọc. Điều này sẽ giúp child element nằm chính giữa parent element.

```css
.parent {
  display: flex;
  justify-content: center;
  align-items: center;
}
```

Sử dụng Grid: Tương tự như Flexbox, ta cũng có thể sử dụng Grid để căn giữa child element trong parent element. Đặt thuộc tính display: grid cho parent element và sử dụng place-items: center để căn giữa cả chiều ngang và chiều dọc.

```css
.parent {
  display: grid;
  place-items: center;
}
```

Sử dụng Position: Đặt thuộc tính position: relative cho parent element và position: absolute cùng với các giá trị top: 50% và left: 50% cho child element. Sau đó, sử dụng transform: translate(-50%, -50%) để căn giữa child element.

```css
.parent {
  position: relative;
}

.child {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}
```

Sử dụng Margin: Đặt margin: auto cho child element. Điều này sẽ tự động căn giữa child element trong parent element, với điều kiện parent element có một kích thước cụ thể.

```css
.child {
  margin: auto;
}
```

## QA12

### Table của css và html khác nhau gì?

**Trả lời:**

Cấu trúc: Trong HTML, table được xây dựng bằng cách sử dụng các thẻ như `<table>, <tr>, <th>,` và `<td>`. Các thẻ này được sử dụng để tạo cấu trúc và xác định các hàng, cột và ô trong table. Trong CSS, không có các thẻ tương tự. CSS chỉ được sử dụng để kiểm soát giao diện và trình bày của table.

Kiểu dáng và giao diện: Trong HTML, kiểu dáng và giao diện của table được xác định chủ yếu bằng cách sử dụng các thuộc tính và giá trị của các thẻ như `<table>, <tr>, <th>`, và `<td>`. Bạn có thể áp dụng các thuộc tính như border, background-color, padding, và text-align cho các thẻ này để tạo kiểu dáng và giao diện của table. Trong CSS, ta có thể sử dụng các thuộc tính như border-collapse, border-spacing, background-color, và text-align để tùy chỉnh kiểu dáng và giao diện của table.

Phần tử và lớp: Trong HTML, bạn có thể sử dụng các phần tử như `<caption>, <thead>, <tbody>,` và `<tfoot>` để cung cấp thông tin bổ sung về table. Bạn cũng có thể sử dụng các lớp để áp dụng kiểu dáng hoặc xác định các nhóm phần tử. Trong CSS, các phần tử như `<caption>, <thead>, <tbody>`, và `<tfoot>` có thể được chọn và tùy chỉnh bằng cách sử dụng các bộ chọn CSS như table caption, table thead, table tbody, và table tfoot. Bạn cũng có thể sử dụng các lớp để chọn và tùy chỉnh các phần tử trong table.

## QA13

### vh, px, vw, em, rem khác nhau gì?

**Trả lời:**

px (pixel): Là đơn vị đo tuyệt đối và không thay đổi. Một pixel tương đương với một điểm ảnh trên màn hình. Khi sử dụng px, kích thước và khoảng cách sẽ được xác định cố định và không thay đổi khi thay đổi kích thước của trình duyệt hoặc thiết bị.

% (percent): Là đơn vị đo tương đối và được tính dựa trên kích thước của phần tử cha. Giá trị % xác định phần trăm của kích thước hoặc khoảng cách so với phần tử cha. Ví dụ, nếu một phần tử có kích thước cha là 100px và bạn đặt width thành 50%, phần tử sẽ có kích thước 50px.

vh (viewport height): Là đơn vị đo tương đối và được tính dựa trên chiều cao của viewport (khu vực hiển thị trên màn hình). Giá trị 1 vh tương đương với 1% chiều cao của viewport. Ví dụ, nếu viewport có chiều cao là 800px, 1 vh sẽ tương đương với 8px.

vw (viewport width): Tương tự như vh, vw là đơn vị đo tương đối và được tính dựa trên chiều rộng của viewport. Giá trị 1 vw tương đương với 1% chiều rộng của viewport.

em: Là đơn vị đo tương đối và được tính dựa trên kích thước của phần tử cha gần nhất hoặc phần tử mà nó được áp dụng. Giá trị em được tính dựa trên font-size của phần tử cha. Ví dụ, nếu phần tử cha có font-size là 16px và bạn đặt font-size của phần tử con là 1.5em, phần tử con sẽ có font-size là 24px (1.5 \* 16px).

rem (root em): Tương tự như em, rem cũng là đơn vị đo tương đối dựa trên kích thước của phần tử root (thường là thẻ `<html>`). Giá trị rem được tính dựa trên font-size của phần tử root. Điểm khác biệt quan trọng giữa rem và em là rem không phụ thuộc vào phần tử cha, mà luôn phụ thuộc vào phần tử root. Điều này giúp giữ cho các giá trị rem nhất quán trên toàn bộ trang web.

## QA14

### set kích thước tối thiểu 1 element kiểu gì?

**Trả lời:**

sử dụng thuộc tính min-width và min-height.

ví dụ

```css
div {
  min-width: 200px;
  min-height: 100px;
}
```

---

# **Train js**

## QA15

### var, let, const phân biệt?

**Trả lời:**

var: Trước khi let và const xuất hiện trong JavaScript, var được sử dụng để khai báo biến. Tuy nhiên, var có một số đặc điểm đáng lưu ý. Một biến được khai báo bằng var có phạm vi chức năng (function scope), nghĩa là biến chỉ tồn tại trong phạm vi của hàm mà nó được khai báo hoặc hàm toàn cục. Ngoài ra, biến khai báo bằng var có thể được khai báo lại và cập nhật giá trị trong cùng một phạm vi.

ví dụ

```javascript
function example() {
  var x = 10;
  if (true) {
    var x = 20;
    console.log(x); // Output: 20
  }
  console.log(x); // Output: 20
}
```

let: let được giới thiệu trong ECMAScript 6 và cung cấp phạm vi khối (block scope). Điều này có nghĩa là biến được khai báo bằng let chỉ tồn tại trong phạm vi của khối mã mà nó được khai báo. Một biến khai báo bằng let không thể được khai báo lại trong cùng một phạm vi, nhưng có thể được cập nhật giá trị.

ví dụ

```javascript
function example() {
  let x = 10;
  if (true) {
    let x = 20;
    console.log(x); // Output: 20
  }
  console.log(x); // Output: 10
}
```

const: const cũng được giới thiệu trong ECMAScript 6 và cũng có phạm vi khối (block scope). Tuy nhiên, const khác với let và var là giá trị của biến không thể được cập nhật sau khi được gán. const tạo một biến chỉ đọc (read-only), nghĩa là bạn không thể gán lại giá trị mới cho biến sau khi nó đã được gán một lần

ví dụ

```javascript
function example() {
  const x = 10;
  x = 20; // Lỗi: Không thể gán lại giá trị cho biến const
}
```

## QA16

### có mấy loại data type?

**Trả lời:**

Có 7 data type hay dùng:

- Chuỗi (String): Được sử dụng để lưu trữ văn bản, chuỗi được đặt trong dấu ngoặc đơn ('') hoặc dấu ngoặc kép (""). Ví dụ: "Hello, world!", 'JavaScript is awesome'.

- Số (Number): Được sử dụng để lưu trữ giá trị số. Số có thể là nguyên (integer) hoặc số thập phân (floating-point). Ví dụ: 42, 3.14, -10.

- Boolean: Được sử dụng để biểu thị giá trị đúng (true) hoặc sai (false). Boolean thường được sử dụng trong các câu lệnh điều kiện và điều khiển luồng chương trình.

- Mảng (Array): Được sử dụng để lưu trữ nhiều giá trị trong một biến. Mảng có thể chứa các phần tử của bất kỳ loại dữ liệu nào và được phân tách bằng dấu phẩy. Ví dụ: [1, 2, 3], ['apple', 'banana', 'orange'].

- Đối tượng (Object): Được sử dụng để lưu trữ thông tin dưới dạng các cặp khóa-giá trị. Mỗi cặp khóa-giá trị được gọi là một thuộc tính của đối tượng. Ví dụ: { name: 'John', age: 25, city: 'New York' }.

- Null: Được sử dụng để biểu thị giá trị không tồn tại hoặc trống rỗng. Khi một biến được gán giá trị null, nó sẽ không tham chiếu đến bất kỳ đối tượng hoặc giá trị nào.

- Undefined: Được sử dụng để biểu thị giá trị chưa được gán hoặc không tồn tại. Khi một biến được khai báo nhưng không được gán giá trị, giá trị mặc định sẽ là undefined.

## QA17

### Điều gì xảy ra khai báo 1 biến:

- Primitive value
- array value
- object value
- object properties
- array element

**Trả lời:**

- Primitive value (giá trị nguyên thủy): Khi bạn khai báo một biến với một giá trị nguyên thủy như chuỗi, số hoặc boolean, giá trị đó sẽ được lưu trữ trực tiếp trong biến. Ví dụ:

```javascript
let name = "John";
let age = 25;
let isStudent = true;
```

- Array value (giá trị mảng): Khi bạn khai báo một biến với giá trị là một mảng, biến sẽ lưu trữ một tham chiếu đến mảng đó. Tham chiếu này cho phép bạn truy cập và thay đổi các phần tử trong mảng. Ví dụ:

```javascript
let numbers = [1, 2, 3, 4, 5];
```

- Object value (giá trị đối tượng): Khi bạn khai báo một biến với giá trị là một đối tượng, biến sẽ lưu trữ một tham chiếu đến đối tượng đó. Tham chiếu này cho phép bạn truy cập và thay đổi các thuộc tính của đối tượng. Ví dụ:

```javascript
let person = { name: "John", age: 25, city: "New York" };
```

- Object properties (thuộc tính đối tượng): Khi bạn truy cập vào một thuộc tính của một đối tượng, bạn sẽ truy cập vào giá trị của thuộc tính đó. Ví dụ:

```javascript
console.log(person.name); // Output: "John"
```

- Array element (phần tử mảng): Khi bạn truy cập vào một phần tử của một mảng, bạn sẽ truy cập vào giá trị của phần tử đó. Ví dụ:

```javascript
console.log(numbers[0]); // Output: 1
```

## QA18

### tạo 1 string và 1 number như thế nào

**Trả lời:**

Để tạo một chuỗi (string) trong JavaScript, bạn chỉ cần sử dụng dấu ngoặc đơn ('') hoặc dấu ngoặc kép ("") để bao quanh văn bản mà bạn muốn tạo thành chuỗi.

ví dụ

```javascript
let myString = "Hello, world!";
```

Để tạo một số (number) trong JavaScript, bạn chỉ cần gán giá trị số cho biến. Số có thể là nguyên (integer) hoặc số thập phân (floating-point).

ví dụ

```javascript
let myNumber = 42;
```

Lưu ý rằng JavaScript là một ngôn ngữ linh hoạt, cho phép bạn gán lại kiểu dữ liệu cho biến dựa trên nội dung của biến đó. Ví dụ, bạn có thể gán một chuỗi cho biến mà trước đó đã được gán giá trị số, và ngược lại. Tuy nhiên, việc này có thể gây nhầm lẫn và khó hiểu mã nguồn của bạn, vì vậy hãy sử dụng các kiểu dữ liệu một cách nhất quán để giữ mã nguồn của bạn dễ đọc và dễ hiểu hơn.

## QA19

### map, filter, slice, includes,... làm quen 1 số method array cơ bản? ví dụ?

**Trả lời:**

map(): Phương thức map() được sử dụng để tạo một mảng mới bằng cách áp dụng một hàm lên từng phần tử của mảng gốc. Nó trả về một mảng mới với các phần tử đã được thay đổi. Ví dụ:

```javascript
const numbers = [1, 2, 3, 4, 5];

const doubledNumbers = numbers.map((num) => num * 2);

console.log(doubledNumbers); // Output: [2, 4, 6, 8, 10]
```

filter(): Phương thức filter() được sử dụng để tạo một mảng mới chỉ chứa các phần tử của mảng gốc mà thỏa mãn một điều kiện được xác định. Nó trả về một mảng mới với các phần tử đã được lọc. Ví dụ:

```javascript
const numbers = [1, 2, 3, 4, 5];

const evenNumbers = numbers.filter((num) => num % 2 === 0);

console.log(evenNumbers); // Output: [2, 4]
```

slice(): Phương thức slice() được sử dụng để tạo một bản sao của một phần của mảng gốc. Nó nhận vào hai tham số là vị trí bắt đầu và vị trí kết thúc (không bao gồm) của mảng mà bạn muốn cắt. Ví dụ:

```javascript
const numbers = [1, 2, 3, 4, 5];

const slicedNumbers = numbers.slice(1, 4);

console.log(slicedNumbers); // Output: [2, 3, 4]
```

includes(): Phương thức includes() được sử dụng để kiểm tra xem một giá trị có tồn tại trong mảng hay không. Nó trả về giá trị boolean (true nếu tồn tại và false nếu không tồn tại). Ví dụ:

```javascript
const numbers = [1, 2, 3, 4, 5];

const hasNumberThree = numbers.includes(3);

console.log(hasNumberThree); // Output: true
```

Trên là 1 số method array thường dùng ngoài ra còn 1 số method hay dùng khác như reduce(), forEach(), sort(), và nhiều hơn nữa. Bạn có thể tham khảo tài liệu JavaScript để tìm hiểu thêm về các phương thức mảng và cách sử dụng chúng.

## QA20

### this là gì? khi nào dùng this? ví dụ

**Trả lời:**

this là một từ khóa đặc biệt mà đại diện cho đối tượng hiện tại mà một phương thức đang được gọi trên, hoặc đối tượng mà một hàm được gọi như một phương thức của nó.

this có thể sử dụng để truy cập và thay đổi các thuộc tính và phương thức của đối tượng đang được tham chiếu.

ví dụ

```javascript
const person = {
  name: "John",
  age: 25,
  greet: function () {
    console.log(
      `Hello, my name is ${this.name} and I am ${this.age} years old.`
    );
  },
};

person.greet(); // Output: Hello, my name is John and I am 25 years old.
```

Trong ví dụ trên, chúng ta có một đối tượng person với hai thuộc tính name và age, cùng với một phương thức greet. Trong phương thức greet, chúng ta sử dụng this để truy cập các thuộc tính name và age của đối tượng person. Khi chúng ta gọi person.greet(), this sẽ trỏ đến đối tượng person, cho phép chúng ta truy cập và sử dụng các thuộc tính của nó.

this cũng có thể được sử dụng trong các hàm xử lý sự kiện hoặc trong các phương thức của các lớp (classes) trong JavaScript.

Lưu ý rằng giá trị của this phụ thuộc vào cách mà một hàm được gọi. Nếu một hàm đơn giản được gọi mà không có đối tượng nào được xác định, this sẽ trỏ đến đối tượng toàn cục (global object) trong trình duyệt hoặc undefined trong chế độ strict mode.

## QA21

### Regex là gì? khi nào dùng?

**Trả lời:**

Regex (Regular Expression) là một chuỗi các ký tự đặc biệt được sử dụng để tìm kiếm, so khớp và thay thế các mẫu trong các chuỗi văn bản. Nó là một công cụ mạnh mẽ để xử lý và kiểm tra các chuỗi dựa trên các quy tắc và mẫu cụ thể.

Trong JavaScript, chúng ta có đối tượng RegExp để tạo và sử dụng biểu thức chính quy. Biểu thức chính quy có thể được sử dụng trong các phương thức như test(), match(), replace(), search(), và split() của chuỗi.

Dưới đây là 1 số trường hợp sử dụng:

- Tìm kiếm và so khớp: Bạn có thể sử dụng Regex để tìm kiếm và so khớp các mẫu trong chuỗi. Ví dụ, bạn có thể kiểm tra xem chuỗi có bắt đầu bằng "Hello" hay không:

```javascript
const str = "Hello, world!";
const pattern = /^Hello/;

console.log(pattern.test(str)); // Output: true
```

- Thay thế và sửa đổi: Bạn có thể sử dụng Regex để thay thế và sửa đổi các mẫu trong chuỗi. Ví dụ, bạn có thể thay thế tất cả các khoảng trắng trong chuỗi bằng dấu gạch dưới:

```javascript
const str = "Hello world!";
const pattern = /\s/g;

const newStr = str.replace(pattern, "_");
console.log(newStr); // Output: Hello_world!
```

- Phân tách chuỗi: Bạn có thể sử dụng Regex để phân tách chuỗi thành các phần tử dựa trên một mẫu cụ thể. Ví dụ, bạn có thể phân tách chuỗi thành một mảng các từ bằng cách sử dụng dấu cách làm dấu phân tách:

```javascript
const str = "Hello world!";
const pattern = /\s/;

const words = str.split(pattern);
console.log(words); // Output: ["Hello", "world!"]
```

## QA22

### Làm sao hanlde error trong js?

**Trả lời:**

Trong JavaScript, để xử lý lỗi (error handling), chúng ta có thể sử dụng câu lệnh `try...catch`. Câu lệnh này cho phép chúng ta thử thực hiện một khối mã và bắt các lỗi xảy ra trong quá trình thực hiện đó.

Khi chúng ta đặt mã có thể gây ra lỗi vào trong khối try, JavaScript sẽ thử thực hiện mã đó. Nếu một lỗi xảy ra trong khối try, nó sẽ được bắt bởi khối catch và mã trong khối catch sẽ được thực thi. Biến error trong khối catch là một đối tượng chứa thông tin về lỗi.

Ngoài ra, chúng ta cũng có thể sử dụng câu lệnh throw để ném một lỗi trong JavaScript. Câu lệnh `throw` cho phép chúng ta tự định nghĩa và ném ra các đối tượng lỗi tùy chỉnh

Lưu ý rằng việc xử lý lỗi là một phần quan trọng trong việc viết mã JavaScript. Bằng cách sử dụng câu lệnh `try...catch` và `throw`, bạn có thể kiểm soát và xử lý các lỗi một cách linh hoạt trong chương trình của mình.

## QA23

### Có mấy dạng scope? Trong trường hợp:

### - x khai báo tại func A thì sử dụng ở đâu

### - x khai báo trong for thì sử dụng ở đâu

### - x khia báo trong if...else dùng ở đâu

**Trả lời:**

Trong JavaScript, có 3 dạng phạm vi (scope) chính: phạm vi toàn cục (global scope), phạm vi hàm (function scope) và phạm vi khối (block scope).

- Phạm vi toàn cục: Khi biến x được khai báo trong phạm vi toàn cục, nghĩa là nó được khai báo bên ngoài bất kỳ hàm, vòng lặp hoặc điều kiện nào. Biến x có thể được sử dụng trong toàn bộ chương trình JavaScript.

```javascript
var x = 10; // biến x được khai báo trong phạm vi toàn cục

function A() {
  console.log(x); // có thể sử dụng biến x ở trong hàm A
}

A(); // in ra giá trị của x
```

- Phạm vi hàm: Khi biến x được khai báo trong một hàm, nghĩa là nó chỉ có thể được sử dụng bên trong hàm đó và không thể truy cập từ bên ngoài hàm.

```javascript
function A() {
  var x = 10; // biến x được khai báo trong phạm vi hàm A
  console.log(x); // có thể sử dụng biến x trong hàm A
}

A(); // in ra giá trị của x
console.log(x); // lỗi: không thể truy cập biến x từ bên ngoài hàm A
```

- Phạm vi khối: Trước ES6, JavaScript không hỗ trợ phạm vi khối. Tuy nhiên, từ ES6 trở đi, chúng ta có thể sử dụng các từ khóa let và const để khai báo biến trong phạm vi khối. Trong trường hợp này, biến x chỉ có thể được sử dụng bên trong khối trong đó nó được khai báo (ví dụ: trong vòng lặp hoặc điều kiện).

```javascript
if (true) {
  let x = 10; // biến x được khai báo trong phạm vi khối của if
  console.log(x); // có thể sử dụng biến x trong if
}

for (let i = 0; i < 5; i++) {
  let x = i; // biến x được khai báo trong phạm vi khối của vòng lặp for
  console.log(x); // có thể sử dụng biến x trong vòng lặp for
}

console.log(x); // lỗi: không thể truy cập biến x từ bên ngoài khối if hoặc vòng lặp for
```

## QA24

### Hoisting là gì? ví dụ?

**Trả lời:**

Hoisting là một tính chất của JavaScript cho phép các khai báo biến và hàm được di chuyển lên đầu phạm vi của chúng trước khi mã được thực thi. Điều này có nghĩa là bạn có thể truy cập vào biến hoặc hàm trước khi chúng được khai báo trong mã.

ví dụ

```javascript
// bình thường
console.log(x); // Kết quả: undefined
var x = 10;
console.log(x); // Kết quả: 10

// áp dụng hoisting
hello(); // Kết quả: "Hello, world!"

function hello() {
  console.log("Hello, world!");
}
```

Lưu ý rằng khai báo biến sử dụng var sẽ được hoist, trong khi khai báo biến sử dụng let và const sẽ không được hoist và chỉ có thể truy cập sau khi chúng được khai báo.

```javascript
console.log(x); // Lỗi: ReferenceError: x is not defined
let x = 10;
console.log(x); // Kết quả: 10
```

## QA25

### Class trong js là gì? cách dùng? construct, methods là gì? inheritance là gì? khi nào dùng?

**Trả lời:**

Trong JavaScript, class là một cách để định nghĩa một đối tượng (object) hoặc một kiểu dữ liệu tùy chỉnh (custom data type). Nó cung cấp một cú pháp rõ ràng và dễ đọc để tạo ra các đối tượng có các thuộc tính (properties) và phương thức (methods) liên quan.

Cách dùng:

- Để tạo một lớp (class) trong JavaScript, chúng ta sử dụng từ khóa class.

```javascript
class ClassName {
  constructor() {
    // Hàm khởi tạo (constructor)
  }

  methodName() {
    // Phương thức (method)
  }
}
```

Trong JavaScript, lớp cũng hỗ trợ tính kế thừa (inheritance). Kế thừa cho phép một lớp con (subclass) kế thừa các thuộc tính và phương thức từ một lớp cha (superclass). Điều này giúp tái sử dụng mã và tạo ra các mối quan hệ phân cấp giữa các lớp. Để kế thừa từ một lớp khác, chúng ta sử dụng từ khóa extends.

```javascript
// superclass
class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  sayHello() {
    console.log(
      `Hello, my name is ${this.name} and I am ${this.age} years old.`
    );
  }
}

// subclass
class Student extends Person {
  constructor(name, age, grade) {
    super(name, age); // Gọi phương thức constructor của lớp cha
    this.grade = grade;
  }

  study() {
    console.log(`${this.name} is studying hard for grade ${this.grade}.`);
  }
}
```

## QA26

### JSON là gì? cách dùng? Tác dụng json trong js? ví dụ.

**Trả lời:**

JSON (JavaScript Object Notation) là một định dạng dữ liệu phổ biến được sử dụng để truyền tải và lưu trữ dữ liệu. Nó được thiết kế để dễ đọc và viết cho con người và dễ dùng cho máy tính để xử lý. JSON sử dụng cú pháp nhẹ nhàng và dựa trên hai cấu trúc cơ bản: các cặp khóa-giá trị và các danh sách giá trị.

Trong JavaScript, chúng ta có thể sử dụng JSON để biểu diễn dữ liệu dưới dạng một đối tượng JavaScript thông qua các kiểu dữ liệu như chuỗi (string), số (number), đối tượng (object), mảng (array), boolean (true/false) và giá trị null.

Để chuyển đổi dữ liệu thành chuỗi JSON, chúng ta có thể sử dụng phương thức JSON.stringify(). Và để chuyển đổi chuỗi JSON thành đối tượng JavaScript, chúng ta có thể sử dụng phương thức JSON.parse().

Tác dụng của JSON trong JavaScript rất đa dạng. Dưới đây là một số tác dụng phổ biến:

- Truyền tải dữ liệu: JSON được sử dụng rộng rãi để truyền tải dữ liệu giữa máy chủ và trình duyệt hoặc giữa các ứng dụng khác nhau thông qua API. Dữ liệu được mã hóa thành chuỗi JSON trước khi được gửi đi và sau đó được giải mã thành đối tượng JavaScript khi nhận được.

- Lưu trữ dữ liệu: JSON có thể được sử dụng để lưu trữ dữ liệu trong cơ sở dữ liệu hoặc tệp tin. Chuỗi JSON có thể được ghi vào tệp tin hoặc lưu trữ trong cơ sở dữ liệu, và sau đó được đọc và chuyển đổi thành đối tượng JavaScript khi cần.

- Giao tiếp với API: JSON là định dạng dữ liệu phổ biến cho việc giao tiếp với các API. Khi gửi yêu cầu HTTP đến một API, dữ liệu thường được trả về dưới dạng chuỗi JSON, sau đó chúng ta có thể chuyển đổi nó thành đối tượng JavaScript để sử dụng.

JSON giúp chúng ta dễ dàng truyền tải, lưu trữ và trao đổi dữ liệu trong JavaScript và các ứng dụng web.

## QA27

### Bất đồng bộ js là gì? Promise, Promise.all, Promise.allSetle là gì? ví dụ?

**Trả lời:**

Bất đồng bộ (asynchronous) trong JavaScript là một khái niệm quan trọng khi làm việc với các tác vụ mà thời gian hoàn thành không được đảm bảo và có thể kéo dài. Trong ngữ cảnh của JavaScript, các tác vụ bất đồng bộ thường là các hoạt động như gửi yêu cầu mạng, đọc/ghi dữ liệu từ cơ sở dữ liệu, hoặc các tác vụ xử lý dữ liệu lớn.

Promise là một cấu trúc dữ liệu trong JavaScript được sử dụng để xử lý các tác vụ bất đồng bộ. Nó đại diện cho một giá trị sẽ có sẵn trong tương lai, có thể là kết quả thành công hoặc lỗi từ một tác vụ bất đồng bộ.

```javascript
function fetchData() {
  return new Promise((resolve, reject) => {
    fetch("https://api.example.com/data")
      .then((response) => response.json())
      .then((data) => resolve(data))
      .catch((error) => reject(error));
  });
}

// Sử dụng Promise
fetchData()
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.log(error);
  });
```

Promise.all là một phương thức tĩnh của lớp Promise trong JavaScript, cho phép chúng ta chờ đợi một mảng các Promise hoàn thành và trả về kết quả của tất cả các Promise đã hoàn thành. Nếu một Promise trong mảng bị từ chối, Promise.all sẽ trả về kết quả từ Promise đầu tiên bị từ chối.

```javascript
const promise1 = Promise.resolve("Hello");
const promise2 = new Promise((resolve, reject) => {
  setTimeout(resolve, 2000, "World");
});
const promise3 = Promise.reject("Oops!");

Promise.all([promise1, promise2])
  .then((values) => {
    console.log(values); // ['Hello', 'World']
  })
  .catch((error) => {
    console.log(error);
  });
```

Promise.allSettled cũng là một phương thức tĩnh của lớp Promise, nhưng nó trả về kết quả cho tất cả các Promise trong mảng, bao gồm cả Promise đã hoàn thành và Promise bị từ chối

```javascript
const promise1 = Promise.resolve("Hello");
const promise2 = Promise.reject("Oops!");
const promise3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 2000, "World");
});

Promise.allSettled([promise1, promise2, promise3]).then((results) => {
  results.forEach((result) => {
    if (result.status === "fulfilled") {
      console.log("Resolved:", result.value);
    } else if (result.status === "rejected") {
      console.log("Rejected:", result.reason);
    }
  });
});
```

## QA28

### Compare async/await và then/catch?

**Trả lời:**

Async/await và then/catch là cách sử dụng Promise trong JavaScript để xử lý mã bất đồng bộ. Dưới đây là một số sự khác biệt giữa chúng:

- Cú pháp: Async/await sử dụng cú pháp gần gũi hơn và dễ đọc hơn so với then/catch. Với async/await, chúng ta có thể viết mã bất đồng bộ giống như viết mã đồng bộ thông qua việc sử dụng các từ khóa async và await. Trong khi đó, với then/catch, chúng ta sử dụng phương thức .then() để xử lý kết quả thành công và phương thức .catch() để xử lý lỗi.

- Luồng xử lý: Với then/catch, các phần xử lý kết quả và xử lý lỗi được xây dựng thành một chuỗi các phương thức .then(). Điều này có thể dẫn đến hiện tượng "callback hell" khi có nhiều tác vụ bất đồng bộ liên tiếp. Trong khi đó, với async/await, chúng ta có thể sử dụng cú pháp tuần tự, giúp mã trở nên dễ đọc hơn và giảm hiện tượng "callback hell".

- Xử lý lỗi: Với then/catch, chúng ta sử dụng phương thức .catch() để xử lý lỗi cho tất cả các phần xử lý kết quả trước đó. Điều này có thể làm cho việc xử lý lỗi trở nên khó khăn khi có nhiều phần xử lý kết quả. Trong khi đó, với async/await, chúng ta có thể sử dụng khối try/catch để xử lý lỗi cho từng phần mã bất đồng bộ riêng biệt. Điều này giúp việc xử lý lỗi trở nên linh hoạt và dễ dàng hơn.

ví dụ

```javascript
// Sử dụng async/await
async function fetchDataAsync() {
  try {
    const response = await fetch("https://api.example.com/data");
    const data = await response.json();
    console.log(data);
  } catch (error) {
    console.log(error);
  }
}

// Sử dụng then/catch
function fetchDataThen() {
  fetch("https://api.example.com/data")
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
    })
    .catch((error) => {
      console.log(error);
    });
}
```

Tổng kết:

async/await và then/catch đều là cách sử dụng Promise trong JavaScript để xử lý mã bất đồng bộ. Async/await cung cấp một cú pháp dễ đọc hơn và giúp giảm hiện tượng "callback hell", trong khi then/catch có cú pháp tường minh hơn và có thể xử lý lỗi cho tất cả các phần xử lý kết quả. Lựa chọn giữa hai phương pháp phụ thuộc vào sự thoải mái và yêu cầu của dự án cụ thể.

## QA29

### Access DOM bằng những func gì? Nên access nhiều lần hay 1 lần? khi loop 1 array cần chú ý gì để tăng performance?

**Trả lời**

Để truy cập DOM trong JavaScript, chúng ta có thể sử dụng các hàm như document.getElementById(), document.querySelector(), và document.querySelectorAll(). Các hàm này giúp chúng ta lấy các phần tử trong DOM dựa trên các selector như ID, class, hoặc tag name.

Khi truy cập DOM, nên cân nhắc số lần truy cập và cách truy cập để tối ưu hiệu suất. Dưới đây là một số nguyên tắc cần lưu ý:

- Truy cập nhiều lần: Nếu chúng ta cần truy cập cùng một phần tử DOM nhiều lần trong mã, nên lưu trữ phần tử đó vào một biến để truy cập nhanh chóng hơn. Việc truy cập DOM có chi phí và tốn thời gian, nên truy cập qua một biến giúp tránh việc lặp lại các truy vấn DOM không cần thiết.

```js
const myElement = document.getElementById("myElement");

// Truy cập nhiều lần
myElement.style.color = "red";
myElement.classList.add("highlight");
```

- Thực hiện lặp qua một mảng: Khi thực hiện lặp qua một mảng các phần tử DOM, nên tránh truy cập DOM trong mỗi vòng lặp. Thay vào đó, nên lấy danh sách các phần tử DOM trước vòng lặp và lưu chúng vào một biến. Điều này giúp tăng hiệu suất bằng cách tránh việc truy cập DOM không cần thiết trong mỗi lần lặp.

```js
const elements = document.querySelectorAll(".myElements");

// Lặp qua mảng phần tử DOM
for (let i = 0; i < elements.length; i++) {
  // Thao tác với phần tử DOM
  elements[i].style.color = "red";
}
```

- Sử dụng phương pháp tối ưu hóa DOM: Khi xử lý DOM trong một lượng lớn dữ liệu, nên sử dụng các phương pháp tối ưu hóa DOM như sử dụng fragment để tạo và cập nhật nhiều phần tử cùng một lúc, hoặc sử dụng requestAnimationFrame để lập lịch các công việc DOM theo chu kỳ làm mới màn hình.

Tổng kết:

- để tăng hiệu suất khi truy cập DOM, nên lưu trữ các phần tử DOM vào biến nếu chúng được sử dụng nhiều lần, tránh truy cập DOM trong mỗi vòng lặp khi lặp qua một mảng, và sử dụng các phương pháp tối ưu hóa DOM khi xử lý dữ liệu lớn.

## QA30

### DOM là gì? có mấy cách reference đến DOM? có mấy cách listen 1 event?

**Trả lời:**

DOM là viết tắt của Document Object Model, và nó là một biểu diễn cấu trúc của HTML hoặc XML dưới dạng một cây đối tượng có thể được truy cập và thao tác bằng JavaScript. DOM biểu thị các phần tử, thuộc tính và văn bản trong tài liệu HTML hoặc XML, cho phép chúng ta thay đổi nội dung, cấu trúc và kiểu dáng của trang web.

Có một số cách để tham chiếu đến DOM trong JavaScript:

- Sử dụng phương thức document.getElementById(): Phương pháp này cho phép chúng ta truy cập đến một phần tử duy nhất trong DOM bằng cách sử dụng giá trị của thuộc tính id của phần tử.

- Sử dụng phương thức document.querySelector(): Phương pháp này cho phép chúng ta truy cập đến một phần tử duy nhất trong DOM bằng cách sử dụng các CSS selector.

- Sử dụng phương thức document.querySelectorAll(): Phương pháp này cho phép chúng ta truy cập đến một danh sách các phần tử trong DOM dựa trên các CSS selector. Kết quả trả về là một NodeList, một đối tượng giống mảng chứa tất cả các phần tử phù hợp.

Đối với việc lắng nghe các sự kiện trong DOM, chúng ta có một số cách:

- Sử dụng phương thức addEventListener(): Phương pháp này cho phép chúng ta gắn một trình xử lý sự kiện vào một phần tử DOM cụ thể. Chúng ta có thể gắn nhiều trình xử lý sự kiện cho cùng một sự kiện trên cùng một phần tử.

- Sử dụng thuộc tính `on[event]`: Một số sự kiện cụ thể có thể được gắn trực tiếp vào một phần tử DOM bằng cách sử dụng thuộc tính `on[event]`, trong đó `[event]` là tên sự kiện. ví dụ: onclick, onblur,...
- Sử dụng phương thức `on[event]`: Tương tự như cách trên, chúng ta có thể gắn một trình xử lý sự kiện trực tiếp vào một phần tử DOM bằng cách sử dụng phương thức `on[event]`, trong đó `[event]` là tên sự kiện.

## QA31

### DOM node là gì? tạo và sử dụng ntn?

**Trả lời:**

DOM node là một thuật ngữ toàn diện hơn, ám chỉ bất kỳ phần tử nào trong cây DOM, bao gồm cả phần tử HTML, văn bản, thuộc tính và các phần tử khác. Mỗi phần tử, văn bản, thuộc tính đều là một DOM node.

Để tạo và sử dụng một DOM node trong JavaScript, chúng ta có thể sử dụng các phương thức và thuộc tính của đối tượng document.

Để tạo một DOM node văn bản, chúng ta có thể sử dụng phương thức createTextNode() của đối tượng document. Phương thức này nhận một tham số là nội dung văn bản và trả về một DOM node mới đại diện cho văn bản đó.

```js
const myText = document.createTextNode("Hello, World!");

const myElement = document.getElementById("myDiv");
myElement.appendChild(myText);
```

Tổng kết:

- để tạo và sử dụng một DOM node, chúng ta có thể sử dụng createElement() để tạo một DOM node mới và sau đó sử dụng các phương thức và thuộc tính để thao tác và điều chỉnh nội dung của nó. Cuối cùng, chúng ta có thể chèn DOM node vào tài liệu HTML bằng cách sử dụng các phương thức như appendChild() hoặc insertBefore().

## QA32

### phân biệt DOM nodes và DOM elements

**Trả lời:**

DOM node là một thuật ngữ toàn diện hơn, ám chỉ bất kỳ phần tử nào trong cây DOM, bao gồm cả phần tử HTML, văn bản, thuộc tính và các phần tử khác. Mỗi phần tử, văn bản, thuộc tính đều là một DOM node.

DOM element là một loại đặc biệt của DOM node, được sử dụng đặc trưng cho các phần tử HTML trong cây DOM. Một DOM element là một đối tượng JavaScript tương ứng với một phần tử HTML cụ thể. Chúng ta có thể truy cập và thao tác các thuộc tính, phương thức, và các sự kiện của phần tử HTML thông qua DOM element.

ví dụ

```js
<div id="myDiv">Hello, World!</div>

<script>
    const myElement = document.getElementById('myDiv');
    myElement.textContent = 'Hello, Universe!';
</script>
```

Trong ví dụ thấy:

- DOM node của phần tử div này là một DOM element. Ta có thể tham chiếu đến DOM element này bằng cách sử dụng các phương thức như getElementById() hoặc querySelector(), và sau đó ta có thể thao tác với nó.
- Ở đây, myElement là một DOM element, đại diện cho phần tử div trong cây DOM. Chúng ta có thể sử dụng DOM element này để truy cập và điều chỉnh các thuộc tính, văn bản và kiểu dáng của phần tử HTML.

Tổng kết:

- DOM node là thuật ngữ chung để ám chỉ bất kỳ phần tử nào trong cây DOM, trong khi DOM element là một loại đặc biệt của DOM node, đại diện cho các phần tử HTML cụ thể trong cây DOM.

## QA33

### ví dụ form validation js

**Trả lời:**

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Form Validation</title>
    <script>
      function validateForm() {
        const name = document.forms["myForm"]["name"].value;
        const email = document.forms["myForm"]["email"].value;

        if (name == "") {
          alert("Please enter your name");
          return false;
        }

        if (email == "") {
          alert("Please enter your email");
          return false;
        }
      }
    </script>
  </head>
  <body>
    <h1>Form Validation</h1>
    <form name="myForm" onsubmit="return validateForm()">
      <label for="name">Name:</label>
      <input type="text" id="name" name="name" required />

      <label for="email">Email:</label>
      <input type="email" id="email" name="email" required />

      <input type="submit" value="Submit" />
    </form>
  </body>
</html>
```

## QA34

### ES6 là gì? compare function và arrow function? Destructing assignment là gì?

**Trả lời:**

ES6, hay còn được gọi là ECMAScript 2015, là một phiên bản tiêu chuẩn mới của ngôn ngữ JavaScript được phát hành vào năm 2015. Nó cung cấp nhiều tính năng mới và cải tiến cho JavaScript, giúp làm cho việc lập trình trở nên dễ dàng và hiệu quả hơn.

So sánh function và arrow function:

- Function là một khối mã JavaScript độc lập có thể được gọi bằng cách sử dụng tên của nó. Nó có thể nhận tham số và trả về giá trị nếu cần.
- Arrow function là một cú pháp viết gọn hơn cho function. Nó sử dụng dấu mũi tên (=>) để định nghĩa hàm. Arrow function không có từ khóa function, không có từ khóa return (nếu chỉ có một biểu thức), và tự động gắn kết this từ phạm vi xung quanh. Arrow function thường được sử dụng để viết mã ngắn gọn và dễ đọc hơn. Nó thích hợp cho các tác vụ đơn giản như xử lý mảng, truy vấn DOM, hoặc xử lý callback.

Destructuring assignment:

- Destructuring assignment là một cú pháp trong ES6 cho phép ta trích xuất các giá trị từ một đối tượng hoặc một mảng và gán chúng vào các biến riêng biệt một cách dễ dàng. Nó giúp ta viết mã ngắn gọn và truy cập dữ liệu thuận tiện hơn.

- Destructuring assignment giúp ta truy cập các giá trị trong đối tượng hoặc mảng một cách dễ dàng và thuận tiện hơn, đồng thời giúp viết mã ngắn gọn và dễ đọc hơn.

ví dụ

```js
const person = {
  name: "John",
  age: 30,
  city: "New York",
};

const { name, age, city } = person;

console.log(name); // 'John'
console.log(age); // 30
console.log(city); // 'New York'
```

---

# **Train react**

## QA35

### Reacjs là gì? Vai trò và tại sao lại dùng react.

## QA36

### Ưu điểm của react so với js?

## QA37

### JSX là gì? ví dụ?

## QA38

### JSX Conditional JSX rendering

## QA39

### React Components là gì? sử dụng làm gì? Component lifecycle là gì?

## QA40

### Tạo component class kiểu gì? ví dụ

## Q41

### Nesting component là gì? ví dụ

## QA42

### Tại sao cần ket khi render component list

## QA43

### Prop và State là gì? compare

## QA44

### Có nên khởi tạo state của component từ props không? tại sao

## QA45

### Controller Component là gì? Ứng dụng

## QA46

### Làm sao để call 1 func sau khi compenent được render/update.

## QA47

### Ref là gì, mục đích dùng

## QA48

### Pros à Cons khi dùng Ref

## QA49

### React Context là gì, sử dụng khi nào?
