// tham khảo: https://www.w3schools.com/nodejs/nodejs_events.asp
// tham khảo: https://freetuts.net/events-trong-nodejs-2186.html
var events = require('events');
var eventEmitter = new events.EventEmitter();

// Tạo xử lý events
var handlerEvent = () => {
    console.log('I hear a scream!');
}

// Chỉ định trình xử lý sự kiện cho một sự kiện:
eventEmitter.on('scream', handlerEvent); // format: eventEmitter.on(eventName, eventHandler);==> eventname: tên event tự định nghĩa, eventHandler: function xử lý

// Kích hoạt sự kiện "scream"
eventEmitter.emit('scream');// eventEmitter.emit(eventname) ==> tên event đã định nghĩa ở on