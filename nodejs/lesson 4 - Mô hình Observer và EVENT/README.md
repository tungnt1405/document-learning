Bài 4:
- Observer là gì?
- Node.js Events là gì?
- Tại sao lại dùng
- ví dụ

Trả lời:
1. Observer là gì?
- Publisher-Subscriber (hay còn được gọi là Observer) là một mô hình thiết kế phần mềm, 
  trong đó các thành phần trong hệ thống giao tiếp với nhau thông qua các sự kiện (events) và đăng ký/lắng nghe các sự kiện đó.
- hệ thống được chia thành hai thành phần chính:
 + Publisher: là đối tượng phát ra các sự kiện (events). Nó không quan tâm đến việc ai đang lắng nghe các sự kiện này.
 + Subscriber: là đối tượng đăng ký để lắng nghe các sự kiện được phát ra bởi Publisher. 
   Subscriber quan tâm đến các sự kiện này và sẽ thực hiện các hành động tương ứng khi có sự kiện xảy ra.
- Một số đặc điểm của mô hình Publisher-Subscriber:
 + Publisher và Subscriber không phụ thuộc lẫn nhau.
 + Publisher không biết gì về Subscriber, nó chỉ phát ra các sự kiện và cho phép các Subscriber đăng ký để lắng nghe các sự kiện đó.
 + Subscriber có thể đăng ký để lắng nghe nhiều sự kiện từ nhiều Publisher khác nhau.
 + Subscriber có thể huỷ đăng ký để không lắng nghe các sự kiện nữa.
2. Node.js Events là gì
- Events là một cơ chế cho phép các đối tượng có thể tạo ra và xử lý các sự kiện (events) trong một ứng dụng.
- Các sự kiện có thể được phát ra từ các đối tượng khác nhau trong ứng dụng, 
  và các đối tượng khác nhau có thể đăng ký (register) để lắng nghe (listen) các sự kiện đó và thực hiện các hành động tương ứng.
- Cơ chế Events trong Node.js được xây dựng trên mô hình Publisher-Subscriber (còn được gọi là Observer). 
  Theo đó, các đối tượng đăng ký để lắng nghe các sự kiện được phát ra từ đối tượng khác, và khi sự kiện đó được phát ra, 
  các đối tượng đăng ký sẽ được thông báo và thực hiện các hành động tương ứng.
3. Tại sao lại dùng
- Khi Node được khởi tạo trên server, đầu tiên nó sẽ khởi tạo các biến, các hàm cần thiết và đợi event xảy ra.
- Nó sẽ giúp Nodejs có thể nhanh hơn các công nghệ khác rất nhiều.
- Nodejs tạo ra một vòng lặp chính để lắng nghe các sự kiện và sau đó nhận lại callback function khi mà sự kiện đó được xác định
- Cơ chế Events trong Node.js rất hữu ích để thiết kế các ứng dụng có tính tương tác cao, ví dụ như các ứng dụng realtime, ứng dụng web (như Express.js) hoặc các ứng dụng đa luồng.