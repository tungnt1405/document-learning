var http = require("http");
var formidable = require("formidable");
var fs = require("fs");
http
  .createServer(function (req, res) {
    if (req.url == "/fileupload") {
      const homepage = { home: req.headers.host };
      var form = new formidable.IncomingForm();
      form.parse(req, function (err, fields, files) {
        var oldpath = files.filetoupload.filepath;
        if (!fs.existsSync("./uploads")) {
          fs.mkdirSync("./uploads");
        }
        var newpath =
          "./uploads/" +
          (Math.random() + 1).toString(36).substring(7) +
          "_" +
          files.filetoupload.originalFilename;
        fs.rename(oldpath, newpath, function (err) {
          if (err) console.error(err);
          res.writeHead(200, { "Content-Type": "application/json" });
          res.end(JSON.stringify({ homepage, fields, files }, null, 2));
        });
      });
    } else {
      res.writeHead(200, { "Content-Type": "text/html" });
      res.write(
        '<form action="fileupload" method="post" enctype="multipart/form-data">'
      );
      res.write('<input type="file" name="filetoupload"><br>');
      res.write('<input type="submit">');
      res.write("</form>");
      return res.end();
    }
  })
  .listen(1234, () => {
    console.log("Server running at http://127.0.0.1:1234/");
  });


