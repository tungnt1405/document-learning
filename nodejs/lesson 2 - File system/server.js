var http = require("http");
var fs = require("fs"); // sử dụng thư viện file system

// File system: append update content to file, if not exist create file same writeFile
fs.appendFile("demo-create2.txt", "Demo created", (err, file) => {
  if (err) throw err;
  console.log("Saved");
}); // Thực hiện 6

// File system: method fopen takes a "flag" as the second argument
// If the flag is "w" for "writing", the specified file is opened for writing.
// If the file does not exist, an empty file is created
fs.open("demo-create.txt", "w", function (err, file) {
  if (err) throw err;
  console.log("Saved2!");
}); // Thực hiện 2

// File system: writeFile update content to file, if not exist create file same appendFile
fs.writeFile("demo-create3.txt", "Nội dung thêm bởi write", function (err) {
  if (err) throw err;
  console.log("Saved3!");
}); // Thực hiện 5

// File system: unlink delete file
fs.unlink("demo-create2.txt", function (err) {
  if (err) throw err;
  console.log("File deleted!");
}); // Thực hiện 3

// File system: rename file
fs.rename("demo-create3.txt", "demo-create4.txt", function (err) {
    if (err) throw err;
    console.log("File Rename!");
  }); // Thực hiện 4

http
  .createServer((req, res) => {
    // File system: read file
    fs.readFile("demo-create.txt", function (err, data) {
      res.writeHead(200, { "Content-Type": "text/html" });
      res.write("Content after create: " + data);
      res.end();
    });
  })
  .listen(1234);

console.log("Server running at http://127.0.0.1:1234/"); // Thực hiện 1
