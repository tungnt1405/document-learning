Bài 2:
- Module fs trong js là gì?
- Tác dụng của module fs với Nodejs
- Cách dùng fs để đọc ghi file trên server bằng Nodejs.
- Tạo ví dụ cho phần này

Trả lời:
1. Module fs trong js là gì?
- Thư viện FileSystem (viết tắt là fs) trong JavaScript là một module được tích hợp sẵn trong Node.js 
  và cung cấp các phương thức để thao tác với hệ thống tập tin của máy tính thông qua mã JavaScript
2. Tác dụng của module fs với Nodejs
- Thư viện fs cho phép bạn đọc/ghi/xóa các tệp tin, tạo thư mục, 
  thực hiện các hoạt động với các đường dẫn và các thuộc tính của các tệp tin và thư mục.
3. Cách dùng fs để đọc ghi file trên server bằng Nodejs.
Các phương thức hay dùng:
- fs.readFile(): đọc nội dung của một tệp tin và trả về dữ liệu đó dưới dạng chuỗi hoặc buffer.
- fs.writeFile(): ghi dữ liệu vào một tệp tin hoặc tạo một tệp tin mới nếu tệp tin đó chưa tồn tại.
- fs.appendFile(): ghi dữ liệu vào cuối tệp tin mà không xóa dữ liệu đã có trong tệp.
- fs.unlink(): xóa một tệp tin.
- fs.mkdir(): tạo một thư mục.
- fs.readdir(): lấy danh sách các tệp tin trong một thư mục.
- fs.stat(): lấy thông tin về một tệp tin hoặc thư mục.
4.Check file server.js