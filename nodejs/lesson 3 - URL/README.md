Bài 3:
- Module URL là gì?
- Tại sao lại dùng URL
- ví dụ

Trả lời:
1. Module URL là gì?
- module URL là một module được tích hợp sẵn trong nodejs và cung cấp các phương thức để phân tích và xử lý các URL (Uniform Resource Locator).
- Module URL cung cấp các phương thức để truy xuất thông tin về các thành phần trong một URL như giao thức, tên miền, đường dẫn, tham số truy vấn và 
  fragment
2. Tại sao lại dùng URL
Một số phương thức quan trọng trong module URL bao gồm:
- url.parse(): phân tích một URL và trả về một đối tượng chứa các thành phần của URL đó.
- url.format(): tạo một URL từ các thành phần được cung cấp.
- url.resolve(): giải quyết một đường dẫn tương đối đến một đường dẫn tuyệt đối hoặc một URL.
Module URL cũng hỗ trợ việc tạo các đường dẫn tương đối và tuyệt đối.
Bạn có thể sử dụng module URL để tạo các URL cho các yêu cầu HTTP, truy vấn cơ sở dữ liệu và các yêu cầu khác trong ứng dụng của mình.