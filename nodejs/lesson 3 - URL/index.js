var http = require("http");
var url = require("url");
var fs = require("fs");

http
  .createServer((req, res) => {
    /**
     * url.parse() là một phương thức của module url trong Node.js.
     * Tham số 1: URL cần phân tích
     * Tham số 2: cho phép phân tích. Nếu đặt là false hoặc không được truyền vào, các tham số truy vấn sẽ được phân tích thành một chuỗi.
     */
    let q = url.parse(req.url, true);
    let fileName = `.${q.pathname}.html`; // tham khảo thêm url.js
    // console.log(fileName);// ./summer.html
    fs.readFile(fileName, (err, data) => {
      if (err) {
        res.writeHead(404, { "Content-Type": "text/html" });
        return res.end("404 Not Found");
      }

      res.writeHead(200, { "Content-Type": "text/html" });
      res.write(data);
      return res.end();
    });
  })
  .listen(1234);

console.log("Server running at http://127.0.0.1:1234/");
