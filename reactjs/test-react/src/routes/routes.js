import { createBrowserRouter } from "react-router-dom";
import config from "../config/";
import InputNumber from "../pages/InputNumber";
import HomePage from "../pages/HomePage";

const publicRouters = createBrowserRouter([
  { path: config.routes.input_number, Component: InputNumber },
  { path: config.routes.home_page, Component: HomePage },
]);

const privateRouters = [];

export { publicRouters, privateRouters };
