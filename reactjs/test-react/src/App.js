import React from "react";
import {
  BrowserRouter,
  Routes,
  Route,
  // RouterProvider
} from "react-router-dom";
import { publicRouters } from "./routes";
import DefaultLayout from "./layouts";

function App() {
  return (
    <React.Fragment>
      <BrowserRouter>
        <Routes>
          {publicRouters.each((route, index) => {
            const Page = route.component;
            let Layout = DefaultLayout;
            if (route.layout) {
              Layout = route.layout;
            }
            // else if (route.layout === null) {
            //   Layout = Fragment;
            // }
            return (
              <Route
                key={index}
                path={route.path}
                element={
                  <Layout>
                    <Page />
                  </Layout>
                }
              />
            );
          })}
        </Routes>
      </BrowserRouter>
    </React.Fragment>
  );
}

export default App;
