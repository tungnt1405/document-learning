import PropTypes from "prop-types";

function DefaultLayout({ children }) {
  return (
    <div>
      <div>
        <div>{children}</div>
      </div>
    </div>
  );
}

DefaultLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default DefaultLayout;
