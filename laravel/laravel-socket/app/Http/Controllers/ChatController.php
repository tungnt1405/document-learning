<?php

namespace App\Http\Controllers;

use App\Events\MessagePosted;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function index() {
        return view('chat');
    }

    public function messages() {
        if (request()->getMethod() === 'POST') {
            $user = Auth::user();
            $data = [
                'message' => request()->get('message', ''),
                'user_id' => $user->id
            ];
            if ($message = Message::create($data)) {
                broadcast(new MessagePosted($message, $user))->toOthers();
                return [
                    'message' => $message->load('user'),
                ];
            }
        }
        return Message::with('user')->get();
    }
}
