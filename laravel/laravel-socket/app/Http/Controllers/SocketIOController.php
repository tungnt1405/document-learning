<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class SocketIOController extends Controller
{
    public function index($any = null)
    {dd(1);
        $http_origin = request()->header('Origin');
        $allowed_origins = explode(', ', env('SOCKET_ALLOWED_ORIGINS'));

        if (in_array($http_origin, $allowed_origins)) {
            $http_host = request()->getHttpHost();

            $redis_client = Redis::connection();

            $redis_client->publish('socketio-io-event', json_encode([
                'host' => $http_host,
                'path' => request()->getPathInfo(),
                'body' => request()->getContent()
            ]));

            return response()->stream(function () use ($redis_client, $http_host, $any) {
                $redis_subcriber = $redis_client->subscribe(['socket-io-response-' . $http_host . '-' . $any], function ($message) {
                    dd($message);
                });
            });
        } else {
            abort(403, 'Forbidden');
        }
    }
}
