<?php

use App\Http\Controllers\ChatController;
use App\Http\Controllers\SocketIOController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('chat');
});

Route::prefix('socket')->group(function() {
    Route::any('/{any}', [SocketIOController::class, 'index'])->where('any', '.*');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// chat realtime
Route::middleware('auth')->group(function() {
    Route::get('chat', [ChatController::class, 'index']);
    Route::match(['get', 'post'], 'messages', [ChatController::class, 'messages']);
    Route::get('getUserLogin', fn() => Auth::user());
});
