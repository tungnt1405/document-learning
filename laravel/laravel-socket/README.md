## sử dụng: 
- First clone the project
- Create .env by running: cp .env.example .env
- Update database configuration in .env to match yours
- Generate project key: php artisan key:generate
- Install dependencies:
``` bash
composer install
npm install
```
- Migrate database: `php artisan migrate`
## Finally start the project, note that each following command needs to run in separate terminal:
```bash
php artisan serve
npm run watch
laravel-echo-server start
php artisan queue:work
```
Open browser at http://localhost:8000 and test
