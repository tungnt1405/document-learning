<?php

use App\Http\Controllers\MessageController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return view('chat');
});

Route::middleware('auth')->group(function() {
    Route::get('chat', [MessageController::class, 'index']);
    Route::match(['get', 'post'], 'messages', [MessageController::class, 'messages']);
    Route::get('getUserLogin', function() {
        return Auth::user();
    });
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
