## sử dụng không dùng docker (tài liệu bên tham khảo [tại đây](https://gitlab.com/tungnt1405/document-learning/-/tree/master/laravel/laravel-socket) )
## sử dụng docker

This project is dockerized to run with non-root user in container (for best security). So first you have to check what is User ID and Group ID of your host user by running the following command:
```bash
id -u
id -g
```
If both of them show `1000`, then simply comment `LOCAL` section in `.docker/laravel-echo-server/Dockerfile` and `Dockerfile` (in root level project), and uncomment PRODUCTION section

Otherwise based on your host User ID and Group ID you need to update those Dockerfiles to match your system UID:GID

After that open `docker-compose.yml` and with service `db` and `redis`, you have to update user with the same UID:GID equal to your host OS

Before start the project, open .env, and update the following:
- `DB_HOST change to db (name of db service in docker-compose.yml)`
- `REDIS_HOST change to redis (name of redis service in docker-compose.yml)`
- `LARAVEL_ECHO_SERVER_REDIS_HOST=redis`
- `APP_PORT=4000`
- `MIX_FRONTEND_PORT=4000`
- `LARAVEL_ECHO_CLIENT=http://localhost:4000/socket.io/socket.io.js`

Start the project:
```bash
docker-compose up -d --build

# Linux (Ubuntu) + MacOS
docker run --rm -v $(pwd):/app -w /app prooph/composer:7.3 install

docker run --rm -v $(pwd):/app -w /app prooph/composer:7.3 dump-autoload

docker run --rm -v $(pwd):/app -w /app node:14-alpine npm install
docker run --rm -v $(pwd):/app -w /app node:14-alpine npm run watch

# Nếu bạn đang dùng Windows thì các command trên sẽ như sau:

# Với Git bash
docker run --rm -v "/$(pwd)":/app -w //app prooph/composer:7.3 install

docker run --rm -v "/$(pwd)":/app -w //app prooph/composer:7.3 dump-autoload

docker run --rm -v "/$(pwd)":/app -w //app node:14-alpine npm install
docker run --rm -v "/$(pwd)":/app -w //app node:14-alpine npm run watch # error

# Với PowerShell
docker run --rm -v "$(pwd):/app" -w /app prooph/composer:7.3 install

docker run --rm -v "$(pwd):/app" -w /app prooph/composer:7.3 dump-autoload

docker run --rm -v "$(pwd):/app" -w /app node:14-alpine npm install
docker run --rm -v "$(pwd):/app" -w /app node:14-alpine npm run watch

# Với Command Prompt
docker run --rm -v "%cd%:/app" -w /app prooph/composer:7.3 install

docker run --rm -v "%cd%:/app" -w /app prooph/composer:7.3 dump-autoload
docker run --rm -v "%cd%:/app" -w /app node:14-alpine npm install
docker run --rm -v "%cd%:/app" -w /app node:14-alpine npm run watch

```

Thực hiện với source code bằng command:
```bash
docker-compose exec -it app [action]
```

mở cmd >**`docker-compose exec -u root app sh`**

Finally open browser at http://localhost:4000 and try


Deloy lên prod [Tham khảo thêm](https://viblo.asia/p/deploy-ung-dung-docker-laravel-realtime-chat-aWj53WJ156m)

Source tham khảo [Tại đây](https://github.com/maitrungduc1410/public-channel-realtime-chat)


## Ngoài ra
In laravel 8 project run following commands to install vue.js
run `composer require laravel/ui`
Install `Vue php artisan ui vue`
Install Vue with auth` php artisan ui vue --auth`
run `npm install && npm run dev`

## Note thêm
```bash
composer install --ignore-platform-reqs
```

- `--ignore-platform-reqs` là một tùy chọn dòng lệnh của Composer cho phép bạn cài đặt một package PHP bất kể các yêu cầu platform của nó. Các yêu cầu platform là các yêu cầu về phiên bản PHP và hệ điều hành mà một package PHP cần để chạy.
