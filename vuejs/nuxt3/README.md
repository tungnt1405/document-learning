# Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# npm
npm install

# pnpm
pnpm install

# yarn
yarn install
```

## **Cách sử dụng nuxtjs 3**

[Tài liệu tham khảo](https://nuxt.com/docs/guide/directory-structure/nuxt) cấu trúc thư mục của nuxt 3

1. **Sửa lại port khởi động tránh bị trùng với các port khác.**

- Mở nuxt.config
- Thêm:

```javascript
...
devServer: {
    port: 9876
}
```

[Tham khảo thêm](https://nuxt.com/docs/api/configuration/nuxt-config)

Trường hợp port sửa mà không nhận thì kiểm tra xem port đó đang sử dụng hay không. Nếu đang sử dụng chọn port khác hoặc tắt đi [Hướng dẫn](https://stackoverflow.com/questions/8688949/how-to-close-tcp-and-udp-ports-via-windows-command-line)

2. **Tạo layouts/default.vue và thêm**

```bash
<template>
    <div>
        <slot/>
    </div>
</template>
```

3. **Sửa lại content app.vue**

```bash
<template>
  <div>
    <NuxtLayout>
      <NuxtPage />
    </NuxtLayout>
  </div>
</template>
```

4. **Tạo pages/index.vue**

```bash
<template>
  <h1>Index page</h1>
</template>
```

5. **Chạy command**

>** Development Server **

Start the development server on `http://localhost:port`:

```bash
# npm
npm run dev

# pnpm
pnpm run dev

# yarn
yarn dev
```

>** Production **

Build the application for production:

```bash
# npm
npm run build

# pnpm
pnpm run build

# yarn
yarn build
```

Locally preview production build:

```bash
# npm
npm run preview

# pnpm
pnpm run preview

# yarn
yarn preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.